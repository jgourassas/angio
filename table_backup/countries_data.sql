--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)

-- Started on 2019-10-23 07:13:59 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3014 (class 0 OID 98750)
-- Dependencies: 209
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.countries (id, value_id, value) VALUES (1, 'AF', 'Afghanistan');
INSERT INTO public.countries (id, value_id, value) VALUES (2, 'AX', 'Åland Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (3, 'AL', 'Albania');
INSERT INTO public.countries (id, value_id, value) VALUES (4, 'DZ', 'Algeria');
INSERT INTO public.countries (id, value_id, value) VALUES (5, 'AS', 'American Samoa');
INSERT INTO public.countries (id, value_id, value) VALUES (6, 'AD', 'Andorra');
INSERT INTO public.countries (id, value_id, value) VALUES (7, 'AO', 'Angola');
INSERT INTO public.countries (id, value_id, value) VALUES (8, 'AI', 'Anguilla');
INSERT INTO public.countries (id, value_id, value) VALUES (9, 'AQ', 'Antarctica');
INSERT INTO public.countries (id, value_id, value) VALUES (10, 'AG', 'Antigua & Barbuda');
INSERT INTO public.countries (id, value_id, value) VALUES (11, 'AR', 'Argentina');
INSERT INTO public.countries (id, value_id, value) VALUES (12, 'AM', 'Armenia');
INSERT INTO public.countries (id, value_id, value) VALUES (13, 'AW', 'Aruba');
INSERT INTO public.countries (id, value_id, value) VALUES (14, 'AC', 'Ascension Island');
INSERT INTO public.countries (id, value_id, value) VALUES (15, 'AU', 'Australia');
INSERT INTO public.countries (id, value_id, value) VALUES (16, 'AT', 'Austria');
INSERT INTO public.countries (id, value_id, value) VALUES (17, 'AZ', 'Azerbaijan');
INSERT INTO public.countries (id, value_id, value) VALUES (18, 'BS', 'Bahamas');
INSERT INTO public.countries (id, value_id, value) VALUES (19, 'BH', 'Bahrain');
INSERT INTO public.countries (id, value_id, value) VALUES (20, 'BD', 'Bangladesh');
INSERT INTO public.countries (id, value_id, value) VALUES (21, 'BB', 'Barbados');
INSERT INTO public.countries (id, value_id, value) VALUES (22, 'BY', 'Belarus');
INSERT INTO public.countries (id, value_id, value) VALUES (23, 'BE', 'Belgium');
INSERT INTO public.countries (id, value_id, value) VALUES (24, 'BZ', 'Belize');
INSERT INTO public.countries (id, value_id, value) VALUES (25, 'BJ', 'Benin');
INSERT INTO public.countries (id, value_id, value) VALUES (26, 'BM', 'Bermuda');
INSERT INTO public.countries (id, value_id, value) VALUES (27, 'BT', 'Bhutan');
INSERT INTO public.countries (id, value_id, value) VALUES (28, 'BO', 'Bolivia');
INSERT INTO public.countries (id, value_id, value) VALUES (29, 'BA', 'Bosnia & Herzegovina');
INSERT INTO public.countries (id, value_id, value) VALUES (30, 'BW', 'Botswana');
INSERT INTO public.countries (id, value_id, value) VALUES (31, 'BR', 'Brazil');
INSERT INTO public.countries (id, value_id, value) VALUES (32, 'IO', 'British Indian Ocean Territory');
INSERT INTO public.countries (id, value_id, value) VALUES (33, 'VG', 'British Virgin Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (34, 'BN', 'Brunei');
INSERT INTO public.countries (id, value_id, value) VALUES (35, 'BG', 'Bulgaria');
INSERT INTO public.countries (id, value_id, value) VALUES (36, 'BF', 'Burkina Faso');
INSERT INTO public.countries (id, value_id, value) VALUES (37, 'BI', 'Burundi');
INSERT INTO public.countries (id, value_id, value) VALUES (38, 'KH', 'Cambodia');
INSERT INTO public.countries (id, value_id, value) VALUES (39, 'CM', 'Cameroon');
INSERT INTO public.countries (id, value_id, value) VALUES (40, 'CA', 'Canada');
INSERT INTO public.countries (id, value_id, value) VALUES (41, 'IC', 'Canary Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (42, 'CV', 'Cape Verde');
INSERT INTO public.countries (id, value_id, value) VALUES (43, 'BQ', 'Caribbean Netherlands');
INSERT INTO public.countries (id, value_id, value) VALUES (44, 'KY', 'Cayman Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (45, 'CF', 'Central African Republic');
INSERT INTO public.countries (id, value_id, value) VALUES (46, 'EA', 'Ceuta & Melilla');
INSERT INTO public.countries (id, value_id, value) VALUES (47, 'TD', 'Chad');
INSERT INTO public.countries (id, value_id, value) VALUES (48, 'CL', 'Chile');
INSERT INTO public.countries (id, value_id, value) VALUES (49, 'CN', 'China');
INSERT INTO public.countries (id, value_id, value) VALUES (50, 'CX', 'Christmas Island');
INSERT INTO public.countries (id, value_id, value) VALUES (51, 'CC', 'Cocos (Keeling) Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (52, 'CO', 'Colombia');
INSERT INTO public.countries (id, value_id, value) VALUES (53, 'KM', 'Comoros');
INSERT INTO public.countries (id, value_id, value) VALUES (54, 'CG', 'Congo - Brazzaville');
INSERT INTO public.countries (id, value_id, value) VALUES (55, 'CD', 'Congo - Kinshasa');
INSERT INTO public.countries (id, value_id, value) VALUES (56, 'CK', 'Cook Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (57, 'CR', 'Costa Rica');
INSERT INTO public.countries (id, value_id, value) VALUES (58, 'CI', 'Côte d’Ivoire');
INSERT INTO public.countries (id, value_id, value) VALUES (59, 'HR', 'Croatia');
INSERT INTO public.countries (id, value_id, value) VALUES (60, 'CU', 'Cuba');
INSERT INTO public.countries (id, value_id, value) VALUES (61, 'CW', 'Curaçao');
INSERT INTO public.countries (id, value_id, value) VALUES (62, 'CY', 'Cyprus');
INSERT INTO public.countries (id, value_id, value) VALUES (63, 'CZ', 'Czechia');
INSERT INTO public.countries (id, value_id, value) VALUES (64, 'DK', 'Denmark');
INSERT INTO public.countries (id, value_id, value) VALUES (65, 'DG', 'Diego Garcia');
INSERT INTO public.countries (id, value_id, value) VALUES (66, 'DJ', 'Djibouti');
INSERT INTO public.countries (id, value_id, value) VALUES (67, 'DM', 'Dominica');
INSERT INTO public.countries (id, value_id, value) VALUES (68, 'DO', 'Dominican Republic');
INSERT INTO public.countries (id, value_id, value) VALUES (69, 'EC', 'Ecuador');
INSERT INTO public.countries (id, value_id, value) VALUES (70, 'EG', 'Egypt');
INSERT INTO public.countries (id, value_id, value) VALUES (71, 'SV', 'El Salvador');
INSERT INTO public.countries (id, value_id, value) VALUES (72, 'GQ', 'Equatorial Guinea');
INSERT INTO public.countries (id, value_id, value) VALUES (73, 'ER', 'Eritrea');
INSERT INTO public.countries (id, value_id, value) VALUES (74, 'EE', 'Estonia');
INSERT INTO public.countries (id, value_id, value) VALUES (75, 'ET', 'Ethiopia');
INSERT INTO public.countries (id, value_id, value) VALUES (76, 'EZ', 'Eurozone');
INSERT INTO public.countries (id, value_id, value) VALUES (77, 'FK', 'Falkland Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (78, 'FO', 'Faroe Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (79, 'FJ', 'Fiji');
INSERT INTO public.countries (id, value_id, value) VALUES (80, 'FI', 'Finland');
INSERT INTO public.countries (id, value_id, value) VALUES (81, 'FR', 'France');
INSERT INTO public.countries (id, value_id, value) VALUES (82, 'GF', 'French Guiana');
INSERT INTO public.countries (id, value_id, value) VALUES (83, 'PF', 'French Polynesia');
INSERT INTO public.countries (id, value_id, value) VALUES (84, 'TF', 'French Southern Territories');
INSERT INTO public.countries (id, value_id, value) VALUES (85, 'GA', 'Gabon');
INSERT INTO public.countries (id, value_id, value) VALUES (86, 'GM', 'Gambia');
INSERT INTO public.countries (id, value_id, value) VALUES (87, 'GE', 'Georgia');
INSERT INTO public.countries (id, value_id, value) VALUES (88, 'DE', 'Germany');
INSERT INTO public.countries (id, value_id, value) VALUES (89, 'GH', 'Ghana');
INSERT INTO public.countries (id, value_id, value) VALUES (90, 'GI', 'Gibraltar');
INSERT INTO public.countries (id, value_id, value) VALUES (91, 'GR', 'Greece');
INSERT INTO public.countries (id, value_id, value) VALUES (92, 'GL', 'Greenland');
INSERT INTO public.countries (id, value_id, value) VALUES (93, 'GD', 'Grenada');
INSERT INTO public.countries (id, value_id, value) VALUES (94, 'GP', 'Guadeloupe');
INSERT INTO public.countries (id, value_id, value) VALUES (95, 'GU', 'Guam');
INSERT INTO public.countries (id, value_id, value) VALUES (96, 'GT', 'Guatemala');
INSERT INTO public.countries (id, value_id, value) VALUES (97, 'GG', 'Guernsey');
INSERT INTO public.countries (id, value_id, value) VALUES (98, 'GN', 'Guinea');
INSERT INTO public.countries (id, value_id, value) VALUES (99, 'GW', 'Guinea-Bissau');
INSERT INTO public.countries (id, value_id, value) VALUES (100, 'GY', 'Guyana');
INSERT INTO public.countries (id, value_id, value) VALUES (101, 'HT', 'Haiti');
INSERT INTO public.countries (id, value_id, value) VALUES (102, 'HN', 'Honduras');
INSERT INTO public.countries (id, value_id, value) VALUES (103, 'HK', 'Hong Kong SAR China');
INSERT INTO public.countries (id, value_id, value) VALUES (104, 'HU', 'Hungary');
INSERT INTO public.countries (id, value_id, value) VALUES (105, 'IS', 'Iceland');
INSERT INTO public.countries (id, value_id, value) VALUES (106, 'IN', 'India');
INSERT INTO public.countries (id, value_id, value) VALUES (107, 'value_id', 'Indonesia');
INSERT INTO public.countries (id, value_id, value) VALUES (108, 'IR', 'Iran');
INSERT INTO public.countries (id, value_id, value) VALUES (109, 'IQ', 'Iraq');
INSERT INTO public.countries (id, value_id, value) VALUES (110, 'IE', 'Ireland');
INSERT INTO public.countries (id, value_id, value) VALUES (111, 'IM', 'Isle of Man');
INSERT INTO public.countries (id, value_id, value) VALUES (112, 'IL', 'Israel');
INSERT INTO public.countries (id, value_id, value) VALUES (113, 'IT', 'Italy');
INSERT INTO public.countries (id, value_id, value) VALUES (114, 'JM', 'Jamaica');
INSERT INTO public.countries (id, value_id, value) VALUES (115, 'JP', 'Japan');
INSERT INTO public.countries (id, value_id, value) VALUES (116, 'JE', 'Jersey');
INSERT INTO public.countries (id, value_id, value) VALUES (117, 'JO', 'Jordan');
INSERT INTO public.countries (id, value_id, value) VALUES (118, 'KZ', 'Kazakhstan');
INSERT INTO public.countries (id, value_id, value) VALUES (119, 'KE', 'Kenya');
INSERT INTO public.countries (id, value_id, value) VALUES (120, 'KI', 'Kiribati');
INSERT INTO public.countries (id, value_id, value) VALUES (121, 'XK', 'Kosovo');
INSERT INTO public.countries (id, value_id, value) VALUES (122, 'KW', 'Kuwait');
INSERT INTO public.countries (id, value_id, value) VALUES (123, 'KG', 'Kyrgyzstan');
INSERT INTO public.countries (id, value_id, value) VALUES (124, 'LA', 'Laos');
INSERT INTO public.countries (id, value_id, value) VALUES (125, 'LV', 'Latvia');
INSERT INTO public.countries (id, value_id, value) VALUES (126, 'LB', 'Lebanon');
INSERT INTO public.countries (id, value_id, value) VALUES (127, 'LS', 'Lesotho');
INSERT INTO public.countries (id, value_id, value) VALUES (128, 'LR', 'Liberia');
INSERT INTO public.countries (id, value_id, value) VALUES (129, 'LY', 'Libya');
INSERT INTO public.countries (id, value_id, value) VALUES (130, 'LI', 'Liechtenstein');
INSERT INTO public.countries (id, value_id, value) VALUES (131, 'LT', 'Lithuania');
INSERT INTO public.countries (id, value_id, value) VALUES (132, 'LU', 'Luxembourg');
INSERT INTO public.countries (id, value_id, value) VALUES (133, 'MO', 'Macau SAR China');
INSERT INTO public.countries (id, value_id, value) VALUES (134, 'MK', 'Macedonia');
INSERT INTO public.countries (id, value_id, value) VALUES (135, 'MG', 'Madagascar');
INSERT INTO public.countries (id, value_id, value) VALUES (136, 'MW', 'Malawi');
INSERT INTO public.countries (id, value_id, value) VALUES (137, 'MY', 'Malaysia');
INSERT INTO public.countries (id, value_id, value) VALUES (138, 'MV', 'Maldives');
INSERT INTO public.countries (id, value_id, value) VALUES (139, 'ML', 'Mali');
INSERT INTO public.countries (id, value_id, value) VALUES (140, 'MT', 'Malta');
INSERT INTO public.countries (id, value_id, value) VALUES (141, 'MH', 'Marshall Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (142, 'MQ', 'Martinique');
INSERT INTO public.countries (id, value_id, value) VALUES (143, 'MR', 'Mauritania');
INSERT INTO public.countries (id, value_id, value) VALUES (144, 'MU', 'Mauritius');
INSERT INTO public.countries (id, value_id, value) VALUES (145, 'YT', 'Mayotte');
INSERT INTO public.countries (id, value_id, value) VALUES (146, 'MX', 'Mexico');
INSERT INTO public.countries (id, value_id, value) VALUES (147, 'FM', 'Micronesia');
INSERT INTO public.countries (id, value_id, value) VALUES (148, 'MD', 'Moldova');
INSERT INTO public.countries (id, value_id, value) VALUES (149, 'MC', 'Monaco');
INSERT INTO public.countries (id, value_id, value) VALUES (150, 'MN', 'Mongolia');
INSERT INTO public.countries (id, value_id, value) VALUES (151, 'ME', 'Montenegro');
INSERT INTO public.countries (id, value_id, value) VALUES (152, 'MS', 'Montserrat');
INSERT INTO public.countries (id, value_id, value) VALUES (153, 'MA', 'Morocco');
INSERT INTO public.countries (id, value_id, value) VALUES (154, 'MZ', 'Mozambique');
INSERT INTO public.countries (id, value_id, value) VALUES (155, 'MM', 'Myanmar (Burma)');
INSERT INTO public.countries (id, value_id, value) VALUES (156, 'NA', 'Namibia');
INSERT INTO public.countries (id, value_id, value) VALUES (157, 'NR', 'Nauru');
INSERT INTO public.countries (id, value_id, value) VALUES (158, 'NP', 'Nepal');
INSERT INTO public.countries (id, value_id, value) VALUES (159, 'NL', 'Netherlands');
INSERT INTO public.countries (id, value_id, value) VALUES (160, 'NC', 'New Caledonia');
INSERT INTO public.countries (id, value_id, value) VALUES (161, 'NZ', 'New Zealand');
INSERT INTO public.countries (id, value_id, value) VALUES (162, 'NI', 'Nicaragua');
INSERT INTO public.countries (id, value_id, value) VALUES (163, 'NE', 'Niger');
INSERT INTO public.countries (id, value_id, value) VALUES (164, 'NG', 'Nigeria');
INSERT INTO public.countries (id, value_id, value) VALUES (165, 'NU', 'Niue');
INSERT INTO public.countries (id, value_id, value) VALUES (166, 'NF', 'Norfolk Island');
INSERT INTO public.countries (id, value_id, value) VALUES (167, 'KP', 'North Korea');
INSERT INTO public.countries (id, value_id, value) VALUES (168, 'MP', 'Northern Mariana Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (169, 'NO', 'Norway');
INSERT INTO public.countries (id, value_id, value) VALUES (170, 'OM', 'Oman');
INSERT INTO public.countries (id, value_id, value) VALUES (171, 'PK', 'Pakistan');
INSERT INTO public.countries (id, value_id, value) VALUES (172, 'PW', 'Palau');
INSERT INTO public.countries (id, value_id, value) VALUES (173, 'PS', 'Palestinian Territories');
INSERT INTO public.countries (id, value_id, value) VALUES (174, 'PA', 'Panama');
INSERT INTO public.countries (id, value_id, value) VALUES (175, 'PG', 'Papua New Guinea');
INSERT INTO public.countries (id, value_id, value) VALUES (176, 'PY', 'Paraguay');
INSERT INTO public.countries (id, value_id, value) VALUES (177, 'PE', 'Peru');
INSERT INTO public.countries (id, value_id, value) VALUES (178, 'PH', 'Philippines');
INSERT INTO public.countries (id, value_id, value) VALUES (179, 'PN', 'Pitcairn Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (180, 'PL', 'Poland');
INSERT INTO public.countries (id, value_id, value) VALUES (181, 'PT', 'Portugal');
INSERT INTO public.countries (id, value_id, value) VALUES (182, 'PR', 'Puerto Rico');
INSERT INTO public.countries (id, value_id, value) VALUES (183, 'QA', 'Qatar');
INSERT INTO public.countries (id, value_id, value) VALUES (184, 'RE', 'Réunion');
INSERT INTO public.countries (id, value_id, value) VALUES (185, 'RO', 'Romania');
INSERT INTO public.countries (id, value_id, value) VALUES (186, 'RU', 'Russia');
INSERT INTO public.countries (id, value_id, value) VALUES (187, 'RW', 'Rwanda');
INSERT INTO public.countries (id, value_id, value) VALUES (188, 'WS', 'Samoa');
INSERT INTO public.countries (id, value_id, value) VALUES (189, 'SM', 'San Marino');
INSERT INTO public.countries (id, value_id, value) VALUES (190, 'ST', 'São Tomé & Príncipe');
INSERT INTO public.countries (id, value_id, value) VALUES (191, 'SA', 'Saudi Arabia');
INSERT INTO public.countries (id, value_id, value) VALUES (192, 'SN', 'Senegal');
INSERT INTO public.countries (id, value_id, value) VALUES (193, 'RS', 'Serbia');
INSERT INTO public.countries (id, value_id, value) VALUES (194, 'SC', 'Seychelles');
INSERT INTO public.countries (id, value_id, value) VALUES (195, 'SL', 'Sierra Leone');
INSERT INTO public.countries (id, value_id, value) VALUES (196, 'SG', 'Singapore');
INSERT INTO public.countries (id, value_id, value) VALUES (197, 'SX', 'Sint Maarten');
INSERT INTO public.countries (id, value_id, value) VALUES (198, 'SK', 'Slovakia');
INSERT INTO public.countries (id, value_id, value) VALUES (199, 'SI', 'Slovenia');
INSERT INTO public.countries (id, value_id, value) VALUES (200, 'SB', 'Solomon Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (201, 'SO', 'Somalia');
INSERT INTO public.countries (id, value_id, value) VALUES (202, 'ZA', 'South Africa');
INSERT INTO public.countries (id, value_id, value) VALUES (203, 'GS', 'South Georgia & South Sandwich Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (204, 'KR', 'South Korea');
INSERT INTO public.countries (id, value_id, value) VALUES (205, 'SS', 'South Sudan');
INSERT INTO public.countries (id, value_id, value) VALUES (206, 'ES', 'Spain');
INSERT INTO public.countries (id, value_id, value) VALUES (207, 'LK', 'Sri Lanka');
INSERT INTO public.countries (id, value_id, value) VALUES (208, 'BL', 'St. Barthélemy');
INSERT INTO public.countries (id, value_id, value) VALUES (209, 'SH', 'St. Helena');
INSERT INTO public.countries (id, value_id, value) VALUES (210, 'KN', 'St. Kitts & Nevis');
INSERT INTO public.countries (id, value_id, value) VALUES (211, 'LC', 'St. Lucia');
INSERT INTO public.countries (id, value_id, value) VALUES (212, 'MF', 'St. Martin');
INSERT INTO public.countries (id, value_id, value) VALUES (213, 'PM', 'St. Pierre & Miquelon');
INSERT INTO public.countries (id, value_id, value) VALUES (214, 'VC', 'St. Vincent & Grenadines');
INSERT INTO public.countries (id, value_id, value) VALUES (215, 'SD', 'Sudan');
INSERT INTO public.countries (id, value_id, value) VALUES (216, 'SR', 'Suriname');
INSERT INTO public.countries (id, value_id, value) VALUES (217, 'SJ', 'Svalbard & Jan Mayen');
INSERT INTO public.countries (id, value_id, value) VALUES (218, 'SZ', 'Swaziland');
INSERT INTO public.countries (id, value_id, value) VALUES (219, 'SE', 'Sweden');
INSERT INTO public.countries (id, value_id, value) VALUES (220, 'CH', 'Switzerland');
INSERT INTO public.countries (id, value_id, value) VALUES (221, 'SY', 'Syria');
INSERT INTO public.countries (id, value_id, value) VALUES (222, 'TW', 'Taiwan');
INSERT INTO public.countries (id, value_id, value) VALUES (223, 'TJ', 'Tajikistan');
INSERT INTO public.countries (id, value_id, value) VALUES (224, 'TZ', 'Tanzania');
INSERT INTO public.countries (id, value_id, value) VALUES (225, 'TH', 'Thailand');
INSERT INTO public.countries (id, value_id, value) VALUES (226, 'TL', 'Timor-Leste');
INSERT INTO public.countries (id, value_id, value) VALUES (227, 'TG', 'Togo');
INSERT INTO public.countries (id, value_id, value) VALUES (228, 'TK', 'Tokelau');
INSERT INTO public.countries (id, value_id, value) VALUES (229, 'TO', 'Tonga');
INSERT INTO public.countries (id, value_id, value) VALUES (230, 'TT', 'Trinidad & Tobago');
INSERT INTO public.countries (id, value_id, value) VALUES (231, 'TA', 'Tristan da Cunha');
INSERT INTO public.countries (id, value_id, value) VALUES (232, 'TN', 'Tunisia');
INSERT INTO public.countries (id, value_id, value) VALUES (233, 'TR', 'Turkey');
INSERT INTO public.countries (id, value_id, value) VALUES (234, 'TM', 'Turkmenistan');
INSERT INTO public.countries (id, value_id, value) VALUES (235, 'TC', 'Turks & Caicos Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (236, 'TV', 'Tuvalu');
INSERT INTO public.countries (id, value_id, value) VALUES (237, 'UM', 'U.S. Outlying Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (238, 'VI', 'U.S. Virgin Islands');
INSERT INTO public.countries (id, value_id, value) VALUES (239, 'UG', 'Uganda');
INSERT INTO public.countries (id, value_id, value) VALUES (240, 'UA', 'Ukraine');
INSERT INTO public.countries (id, value_id, value) VALUES (241, 'AE', 'United Arab Emirates');
INSERT INTO public.countries (id, value_id, value) VALUES (242, 'GB', 'United Kingdom');
INSERT INTO public.countries (id, value_id, value) VALUES (243, 'UN', 'United Nations');
INSERT INTO public.countries (id, value_id, value) VALUES (244, 'US', 'United States');
INSERT INTO public.countries (id, value_id, value) VALUES (245, 'UY', 'Uruguay');
INSERT INTO public.countries (id, value_id, value) VALUES (246, 'UZ', 'Uzbekistan');
INSERT INTO public.countries (id, value_id, value) VALUES (247, 'VU', 'Vanuatu');
INSERT INTO public.countries (id, value_id, value) VALUES (248, 'VA', 'Vatican City');
INSERT INTO public.countries (id, value_id, value) VALUES (249, 'VE', 'Venezuela');
INSERT INTO public.countries (id, value_id, value) VALUES (250, 'VN', 'Vietnam');
INSERT INTO public.countries (id, value_id, value) VALUES (251, 'WF', 'Wallis & Futuna');
INSERT INTO public.countries (id, value_id, value) VALUES (252, 'EH', 'Western Sahara');
INSERT INTO public.countries (id, value_id, value) VALUES (253, 'YE', 'Yemen');
INSERT INTO public.countries (id, value_id, value) VALUES (254, 'ZM', 'Zambia');
INSERT INTO public.countries (id, value_id, value) VALUES (255, 'ZW', 'Zimbabwe');


--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 208
-- Name: countries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.countries_id_seq', 255, true);


-- Completed on 2019-10-23 07:14:00 EEST

--
-- PostgreSQL database dump complete
--

