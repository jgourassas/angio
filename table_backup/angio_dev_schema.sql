--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cath_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cath_events (
    id bigint NOT NULL,
    cath_ev_header character varying(50),
    cath_ev_event_yn boolean DEFAULT false NOT NULL,
    cath_ev_expired_yn boolean DEFAULT false NOT NULL,
    cath_ev_death_at_proc_yn boolean DEFAULT false NOT NULL,
    cath_ev_infarction_yn boolean DEFAULT false NOT NULL,
    res_term_infarction_type character varying(50),
    cath_ev_prior_mi_yn boolean DEFAULT false NOT NULL,
    cath_ev_shock_start_yn boolean DEFAULT false NOT NULL,
    cath_ev_shock_cath_induced_yn boolean DEFAULT false NOT NULL,
    cath_ev_shock_new_post_yn boolean DEFAULT false NOT NULL,
    cath_ev_cardiac_perforation_yn boolean DEFAULT false NOT NULL,
    uni_cardiac_perforation_site character varying(50),
    cath_ev_tamponate_yn boolean DEFAULT false NOT NULL,
    cath_ev_chf_new_post_yn boolean DEFAULT false NOT NULL,
    cath_ev_cor_art_perforation_yn boolean DEFAULT false NOT NULL,
    cath_ev_cor_art_dissection_yn boolean DEFAULT false NOT NULL,
    cath_ev_cor_vein_dissection_yn boolean DEFAULT false NOT NULL,
    cath_ev_cor_art_thrombus_yn boolean DEFAULT false NOT NULL,
    cath_ev_stroke_new_yn boolean DEFAULT false NOT NULL,
    cath_ev_stroke_new_hemo_yn boolean DEFAULT false NOT NULL,
    cath_ev_stroke_new_24h_yn boolean DEFAULT false NOT NULL,
    cath_ev_stroke_prior_yn boolean DEFAULT false NOT NULL,
    uni_dialysis_new_req_yn boolean DEFAULT false NOT NULL,
    uni_dialysis_new_req_dt date,
    uni_currently_on_dialysis boolean DEFAULT false NOT NULL,
    cath_ev_bleed_yn boolean DEFAULT false NOT NULL,
    cath_ev_bleed_dt date,
    cath_ev_bleed_tm time(0) without time zone,
    cath_ev_bleed_dt_tm_utc timestamp(0) without time zone,
    cath_ev_blood_transfusion_yn boolean DEFAULT false NOT NULL,
    cath_ev_hb_prior_transfer boolean DEFAULT false NOT NULL,
    cath_ev_bleed_access_yn boolean DEFAULT false NOT NULL,
    cath_ev_bleed_gu_yn boolean DEFAULT false NOT NULL,
    cath_ev_bleed_gi_yn boolean DEFAULT false NOT NULL,
    cath_ev_bleed_retroperi_yn boolean DEFAULT false NOT NULL,
    cath_ev_bleed_other_yn boolean DEFAULT false NOT NULL,
    cath_ev_vasc_compl_yn boolean DEFAULT false NOT NULL,
    cath_ev_hematoma_at_access_yn boolean DEFAULT false NOT NULL,
    cath_ev_hematoma_size character varying(50),
    cath_ev_hematoma_treat character varying(50),
    cath_ev_vasc_pseudoaneurysm_yn boolean DEFAULT false NOT NULL,
    cath_ev_vasc_dissection_yn boolean DEFAULT false NOT NULL,
    cath_ev_av_fistula_yn boolean DEFAULT false NOT NULL,
    cath_ev_vasc_limb_ischemia_yn boolean DEFAULT false NOT NULL,
    cath_ev_for_cabg_indication character varying(50),
    cath_ev_for_cabg_status character varying(50),
    cath_ev_cabg_location character varying(50),
    cath_ev_cabg_dt date,
    cath_ev_cabg_tm time(0) without time zone,
    cath_ev_cabg_dt_tm_utc timestamp(0) without time zone,
    cath_ev_valve_injury_yn boolean DEFAULT false NOT NULL,
    cath_ev_heart_block_yn boolean DEFAULT false NOT NULL,
    cath_ev_cardioversion_yn boolean DEFAULT false NOT NULL,
    cath_event_notes text,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cath_events OWNER TO postgres;

--
-- Name: cath_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cath_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cath_events_id_seq OWNER TO postgres;

--
-- Name: cath_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cath_events_id_seq OWNED BY public.cath_events.id;


--
-- Name: cath_graft; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cath_graft (
    id bigint NOT NULL,
    cath_graft_header character varying(255),
    graft_morphology character varying(255),
    graft_index character varying(255),
    graft_total_occlusion_yn boolean DEFAULT false NOT NULL,
    graft_sten_yn boolean DEFAULT false NOT NULL,
    graft_ostial_sten_yn boolean DEFAULT false NOT NULL,
    graft_ostial_sten_pct character varying(255),
    graft_ostial_device_present_yn boolean DEFAULT false NOT NULL,
    graft_ostial_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_ostial_prior_treated_dt date,
    graft_ostial_sten_ffr character varying(255),
    graft_ostial_sten_ivus character varying(255),
    graft_prox_sten_yn boolean DEFAULT false NOT NULL,
    graft_prox_sten_pct character varying(255),
    graft_prox_sten_device_present_yn boolean DEFAULT false NOT NULL,
    graft_prox_sten_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_prox_sten_prior_treated_dt date,
    graft_prox_sten_ffr character varying(255),
    graft_prox_sten_ivus character varying(255),
    graft_mid_sten_yn boolean DEFAULT false NOT NULL,
    graft_mid_sten_pct character varying(255),
    graft_mid_sten_device_present_yn boolean DEFAULT false NOT NULL,
    graft_mid_sten_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_mid_sten_prior_treated_dt date,
    graft_mid_sten_ffr character varying(255),
    graft_mid_sten_ivus character varying(255),
    graft_dist_sten_yn boolean DEFAULT false NOT NULL,
    graft_dist_sten_pct character varying(255),
    graft_dist_sten_device_present_yn boolean DEFAULT false NOT NULL,
    graft_dist_sten_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_dist_sten_prior_treated_dt date,
    graft_dist_sten_ffr character varying(255),
    graft_dist_sten_ivus character varying(255),
    graft_dist_anast_sten_yn boolean DEFAULT false NOT NULL,
    graft_dist_anast_type character varying(255),
    graft_dist_anast_device_present_yn boolean DEFAULT false NOT NULL,
    graft_dist_anast_sten_pct character varying(255),
    graft_dist_anast_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_dist_anast_prior_treated_dt date,
    graft_dist_anast_sten_ffr character varying(255),
    graft_dist_anast_sten_ivus character varying(255),
    cath_graft_prox_site character varying(255),
    cath_graft_conduit_type character varying(255),
    cath_graft_segments_main_vessel character varying(255),
    cath_graft_segments_ncdr character varying(255)[],
    cath_graft_segments_cdisc character varying(255)[],
    cath_graft_segments_sts character varying(255)[],
    patient_id integer,
    graft_notes text,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cath_graft OWNER TO postgres;

--
-- Name: cath_graft_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cath_graft_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cath_graft_id_seq OWNER TO postgres;

--
-- Name: cath_graft_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cath_graft_id_seq OWNED BY public.cath_graft.id;


--
-- Name: cath_grafts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cath_grafts (
    id bigint NOT NULL,
    cath_graft_header character varying(255),
    graft_morphology character varying(255),
    graft_index character varying(255),
    graft_total_occlusion_yn boolean DEFAULT false NOT NULL,
    graft_sten_yn boolean DEFAULT false NOT NULL,
    graft_ostial_sten_yn boolean DEFAULT false NOT NULL,
    graft_ostial_sten_pct character varying(255),
    graft_ostial_device_present_yn boolean DEFAULT false NOT NULL,
    graft_ostial_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_ostial_prior_treated_dt date,
    graft_ostial_sten_ffr character varying(255),
    graft_ostial_sten_ivus character varying(255),
    graft_prox_sten_yn boolean DEFAULT false NOT NULL,
    graft_prox_sten_pct character varying(255),
    graft_prox_sten_device_present_yn boolean DEFAULT false NOT NULL,
    graft_prox_sten_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_prox_sten_prior_treated_dt date,
    graft_prox_sten_ffr character varying(255),
    graft_prox_sten_ivus character varying(255),
    graft_mid_sten_yn boolean DEFAULT false NOT NULL,
    graft_mid_sten_pct character varying(255),
    graft_mid_sten_device_present_yn boolean DEFAULT false NOT NULL,
    graft_mid_sten_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_mid_sten_prior_treated_dt date,
    graft_mid_sten_ffr character varying(255),
    graft_mid_sten_ivus character varying(255),
    graft_dist_sten_yn boolean DEFAULT false NOT NULL,
    graft_dist_sten_pct character varying(255),
    graft_dist_sten_device_present_yn boolean DEFAULT false NOT NULL,
    graft_dist_sten_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_dist_sten_prior_treated_dt date,
    graft_dist_sten_ffr character varying(255),
    graft_dist_sten_ivus character varying(255),
    graft_dist_anast_sten_yn boolean DEFAULT false NOT NULL,
    graft_dist_anast_type character varying(255),
    graft_dist_anast_device_present_yn boolean DEFAULT false NOT NULL,
    graft_dist_anast_sten_pct character varying(255),
    graft_dist_anast_prior_treated_yn boolean DEFAULT false NOT NULL,
    graft_dist_anast_prior_treated_dt date,
    graft_dist_anast_sten_ffr character varying(255),
    graft_dist_anast_sten_ivus character varying(255),
    cath_graft_prox_site character varying(255),
    cath_graft_conduit_type character varying(255),
    cath_graft_segments_main_vessel character varying(255),
    cath_graft_segments_ncdr character varying(255)[],
    cath_graft_segments_cdisc character varying(255)[],
    cath_graft_segments_sts character varying(255)[],
    patient_id integer,
    graft_notes text,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cath_grafts OWNER TO postgres;

--
-- Name: cath_grafts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cath_grafts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cath_grafts_id_seq OWNER TO postgres;

--
-- Name: cath_grafts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cath_grafts_id_seq OWNED BY public.cath_grafts.id;


--
-- Name: cath_meds; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cath_meds (
    id bigint NOT NULL,
    cmed_anti_anginal_yn boolean DEFAULT false NOT NULL,
    cmed_anti_anginal_other_yn boolean DEFAULT false NOT NULL,
    cmed_antiarrhythmics_yn boolean DEFAULT false NOT NULL,
    cmed_anticoagulants_yn boolean DEFAULT false NOT NULL,
    cmed_antihypertensives_yn boolean DEFAULT false NOT NULL,
    cmed_antiplatelets_yn boolean DEFAULT false NOT NULL,
    cmed_aspirin_yn boolean DEFAULT false NOT NULL,
    cmed_atropine_yn boolean DEFAULT false NOT NULL,
    cmed_bblocker_yn boolean DEFAULT false NOT NULL,
    cmed_ca_channel_pre_yn boolean DEFAULT false NOT NULL,
    cmed_direct_thrombin character varying(50),
    cmed_diuretics_yn boolean DEFAULT false NOT NULL,
    cmed_gp_iib_iiia_yn boolean DEFAULT false NOT NULL,
    cmed_inotrops_yn boolean DEFAULT false NOT NULL,
    cmed_lytics_bolus_dt date,
    cmed_lytics_bolus_tm time(0) without time zone,
    cmed_lytics_yn boolean DEFAULT false NOT NULL,
    cmed_pde_inhibitor_yn boolean DEFAULT false NOT NULL,
    cmed_prostaglandins_yn boolean DEFAULT false NOT NULL,
    cmed_vaso_dilators_yn boolean DEFAULT false NOT NULL,
    cmed_nitrates_long_act_yn boolean DEFAULT false NOT NULL,
    cmed_ranolazine_yn boolean DEFAULT false NOT NULL,
    cmed_ufh_dose character varying(50),
    cmed_ufh_yn boolean DEFAULT false NOT NULL,
    cmed_thienopyridines character varying(50),
    cmed_xa_inhibitors character varying(50),
    cmed_ace_yn boolean DEFAULT false NOT NULL,
    cmed_arb_yn boolean DEFAULT false NOT NULL,
    cmed_statin_yn boolean DEFAULT false NOT NULL,
    cmed_no_statin_agent_yn boolean DEFAULT false NOT NULL,
    cmed_lmwh_yn boolean DEFAULT false NOT NULL,
    cmed_medications_other_yn boolean DEFAULT false NOT NULL,
    cmed_meds_select character varying(50),
    cmed_notes text,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cath_meds OWNER TO postgres;

--
-- Name: cath_meds_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cath_meds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cath_meds_id_seq OWNER TO postgres;

--
-- Name: cath_meds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cath_meds_id_seq OWNED BY public.cath_meds.id;


--
-- Name: cath_radiations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cath_radiations (
    id bigint NOT NULL,
    car_area_product_units character varying(255),
    car_comulative_kerma character varying(255),
    car_dose_area_meas_method character varying(255),
    car_dose_area_product character varying(255),
    car_fluoro_time character varying(255),
    car_notes text,
    patient_id integer,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cath_radiations OWNER TO postgres;

--
-- Name: cath_radiations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cath_radiations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cath_radiations_id_seq OWNER TO postgres;

--
-- Name: cath_radiations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cath_radiations_id_seq OWNED BY public.cath_radiations.id;


--
-- Name: cor_anatomies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cor_anatomies (
    id bigint NOT NULL,
    cor_anat_variations_yn boolean DEFAULT false NOT NULL,
    cor_anat_anomalies_yn boolean DEFAULT false NOT NULL,
    cor_anat_dominance character varying(50),
    cor_anat_lm_ostium_type character varying(50),
    cor_anat_lm_type character varying(50),
    cor_anat_rca_type character varying(50),
    cor_anat_rca_ostium_type character varying(50),
    cor_anat_rca_mid_type character varying(50),
    cor_anat_rca_rv_branch_type character varying(50),
    cor_anat_rca_distal_type character varying(50),
    cor_anat_rca_pda_type character varying(50),
    cor_anat_lad_ostium_type character varying(50),
    cor_anat_lad_prox_type character varying(50),
    cor_anat_lad_mid_type character varying(50),
    cor_anat_septal_ostium_type character varying(50),
    cor_anat_lad_distal_type character varying(50),
    cor_anat_lad_d1_type character varying(50),
    cor_anat_lad_d2_type character varying(50),
    cor_anat_lad_d3_type character varying(50),
    cor_anat_sino_atrial_type character varying(50),
    cor_anat_lcx_type character varying(50),
    cor_anat_lcx_ostium_type character varying(50),
    cor_anat_lcx_prox_type character varying(50),
    cor_anat_lcx_mid_type character varying(50),
    cor_anat_lcx_distal_type character varying(50),
    cor_anat_lcx_om1_type character varying(50),
    cor_anat_lcx_om2_type character varying(50),
    cor_anat_lcx_om3_type character varying(50),
    cor_anat_ramus_type character varying(50),
    cor_anat_pl_type character varying(50),
    cor_anat_conus_type character varying(50),
    cor_anat_notes text,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cor_anatomies OWNER TO postgres;

--
-- Name: cor_anatomies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cor_anatomies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cor_anatomies_id_seq OWNER TO postgres;

--
-- Name: cor_anatomies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cor_anatomies_id_seq OWNED BY public.cor_anatomies.id;


--
-- Name: cor_collaterals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cor_collaterals (
    id bigint NOT NULL,
    collat_yn boolean DEFAULT false NOT NULL,
    collat_donor_seg character varying(50),
    collat_recipient_seg character varying(50),
    collat_via_seg character varying(50),
    collat_rentrop_grade character varying(50),
    collat_flow_idx_calc_yn boolean DEFAULT false NOT NULL,
    collat_flow_idx_method character varying(50),
    collat_flow_idx_result character varying(50),
    collat_qualitative_class character varying(50),
    collat_notes text,
    patient_id integer,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.cor_collaterals OWNER TO postgres;

--
-- Name: cor_collaterals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cor_collaterals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cor_collaterals_id_seq OWNER TO postgres;

--
-- Name: cor_collaterals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cor_collaterals_id_seq OWNED BY public.cor_collaterals.id;


--
-- Name: cor_lesions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cor_lesions (
    id bigint NOT NULL,
    cor_les_pct character varying(10),
    cor_les_major_branch character varying(10),
    cor_les_seg_ncdr character varying(255)[],
    cor_les_seg_cdisc character varying(255)[],
    cor_les_seg_sts character varying(255)[],
    cor_les_vessel_size character varying(10),
    cor_les_type_scai character varying(10),
    cor_les_type_aha character varying(10),
    cor_les_length character varying(10),
    cor_les_chronic_total_yn boolean DEFAULT false NOT NULL,
    cor_les_calcium_yn boolean DEFAULT false NOT NULL,
    cor_les_thrombus_yn boolean DEFAULT false NOT NULL,
    cor_les_bifurcation_yn boolean DEFAULT false NOT NULL,
    cor_les_tandem_yn boolean DEFAULT false NOT NULL,
    cor_les_aneurysm_yn boolean DEFAULT false NOT NULL,
    cor_les_protected_yn boolean DEFAULT false NOT NULL,
    cor_les_graft_pct character varying(10),
    cor_les_prior_treated_yn boolean DEFAULT false NOT NULL,
    cor_les_prior_treated_dt date,
    cor_les_prior_stent_yn boolean DEFAULT false NOT NULL,
    cor_les_stent_dt date,
    cor_les_stent_pct character varying(10),
    cor_les_ffr character varying(10),
    cor_les_ivus character varying(10),
    cor_les_counter integer,
    patient_id integer,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    cor_les_ostial_loc_yn boolean DEFAULT false NOT NULL,
    cor_les_prior_treated_other character varying(50),
    cor_les_notes text
);


ALTER TABLE public.cor_lesions OWNER TO postgres;

--
-- Name: cor_lesions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cor_lesions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cor_lesions_id_seq OWNER TO postgres;

--
-- Name: cor_lesions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cor_lesions_id_seq OWNED BY public.cor_lesions.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.countries (
    id bigint NOT NULL,
    value_id character varying(10),
    value character varying(64)
);


ALTER TABLE public.countries OWNER TO postgres;

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.countries_id_seq OWNER TO postgres;

--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;


--
-- Name: definitions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.definitions (
    id bigint NOT NULL,
    definition_id integer,
    table_name character varying(255),
    name character varying(255),
    coding_instructions text,
    target_value text,
    support_definition text,
    notes text,
    ref character varying(255),
    vocabulary_en character varying(255),
    title_name character varying(255),
    short_name character varying(255),
    default_value character varying(255),
    usual_range character varying(255),
    valid_range character varying(255),
    data_source character varying(255),
    defs_table_id integer,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    defs_table_id_multi integer[]
);


ALTER TABLE public.definitions OWNER TO postgres;

--
-- Name: definitions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.definitions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.definitions_id_seq OWNER TO postgres;

--
-- Name: definitions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.definitions_id_seq OWNED BY public.definitions.id;


--
-- Name: defs_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.defs_codes (
    id bigint NOT NULL,
    code character varying(255),
    code_system character varying(255),
    description text,
    note text,
    definition_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.defs_codes OWNER TO postgres;

--
-- Name: defs_codes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.defs_codes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.defs_codes_id_seq OWNER TO postgres;

--
-- Name: defs_codes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.defs_codes_id_seq OWNED BY public.defs_codes.id;


--
-- Name: defs_options; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.defs_options (
    id bigint NOT NULL,
    name character varying(255),
    code character varying(255),
    selection_text text,
    selection_definition text,
    selection_notes text,
    definition_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.defs_options OWNER TO postgres;

--
-- Name: defs_options_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.defs_options_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.defs_options_id_seq OWNER TO postgres;

--
-- Name: defs_options_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.defs_options_id_seq OWNED BY public.defs_options.id;


--
-- Name: defs_sentences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.defs_sentences (
    id bigint NOT NULL,
    language character varying(2),
    sentence text,
    note text,
    definition_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.defs_sentences OWNER TO postgres;

--
-- Name: defs_sentences_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.defs_sentences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.defs_sentences_id_seq OWNER TO postgres;

--
-- Name: defs_sentences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.defs_sentences_id_seq OWNED BY public.defs_sentences.id;


--
-- Name: defs_tables; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.defs_tables (
    id bigint NOT NULL,
    cda_xml text,
    code character varying(255),
    code_system character varying(255),
    code_system_name character varying(255),
    display_name character varying(255),
    name character varying(255),
    note character varying(255),
    template_id character varying(255),
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.defs_tables OWNER TO postgres;

--
-- Name: defs_tables_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.defs_tables_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.defs_tables_id_seq OWNER TO postgres;

--
-- Name: defs_tables_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.defs_tables_id_seq OWNED BY public.defs_tables.id;


--
-- Name: info_coronaries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.info_coronaries (
    id bigint NOT NULL,
    info_cor_diagnostic_status character varying(255),
    info_cor_date date,
    info_cor_arrival_dt date,
    info_cor_arrival_tm time(0) without time zone,
    info_cor_start_tm time(0) without time zone,
    info_cor_end_tm time(0) without time zone,
    info_exam_num character varying(50),
    info_cor_main_reason character varying(50),
    icd10cm_code_1 character varying(50),
    icd10cm_code_2 character varying(50),
    icd10cm_code_3 character varying(50),
    icd10cm_code_4 character varying(50),
    info_cor_normal_natives_yn boolean DEFAULT false NOT NULL,
    info_cor_anomalies_yn boolean DEFAULT false NOT NULL,
    info_cor_dis_ves_num character varying(50),
    info_cor_prior_interv_yn boolean DEFAULT false NOT NULL,
    info_cor_grafts_yn boolean DEFAULT false NOT NULL,
    info_cor_grafts_dis_yn boolean DEFAULT false NOT NULL,
    info_cor_cabg_prior_dt date,
    info_cor_grafts_implanted_num character varying(50),
    info_cor_grafts_patent_num character varying(50),
    info_cor_rx_recommend text,
    info_cor_notes text,
    cor_les_count character varying(255),
    patient_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.info_coronaries OWNER TO postgres;

--
-- Name: info_coronaries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.info_coronaries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.info_coronaries_id_seq OWNER TO postgres;

--
-- Name: info_coronaries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.info_coronaries_id_seq OWNED BY public.info_coronaries.id;


--
-- Name: languages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.languages (
    id bigint NOT NULL,
    english character varying(255),
    alpha2 character varying(255)
);


ALTER TABLE public.languages OWNER TO postgres;

--
-- Name: languages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.languages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.languages_id_seq OWNER TO postgres;

--
-- Name: languages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.languages_id_seq OWNED BY public.languages.id;


--
-- Name: lventricles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lventricles (
    id bigint NOT NULL,
    cath_lv_normal_yn boolean DEFAULT false NOT NULL,
    cath_lv_ef_val character varying(50),
    cath_lv_ef_method character varying(50),
    cath_lv_heart_rate character varying(50),
    cath_lv_lv_pres_sys character varying(50),
    cath_lv_lv_pres_end_dias character varying(50),
    cath_lv_mvr character varying(50),
    cath_lv_rao_antero_basal character varying(50),
    cath_lv_rao_antero_lat character varying(50),
    cath_lv_rao_apical character varying(50),
    cath_lv_rao_diaphragmatic character varying(50),
    cath_lv_rao_post_basal character varying(50),
    cath_lv_lao_basal_septal character varying(50),
    cath_lv_lao_apical_septal character varying(50),
    cath_lv_lao_post_lat character varying(50),
    cath_lv_lao_inf_lat character varying(50),
    cath_lv_lao_super_lat character varying(50),
    cath_lv_wall_seg_func character varying(50),
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    cath_lv_notes text,
    aortic_pres_sys character varying(255),
    aortic_pres_dia character varying(255)
);


ALTER TABLE public.lventricles OWNER TO postgres;

--
-- Name: lventricles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lventricles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lventricles_id_seq OWNER TO postgres;

--
-- Name: lventricles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lventricles_id_seq OWNED BY public.lventricles.id;


--
-- Name: opts_codes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opts_codes (
    id bigint NOT NULL,
    code_system character varying(255),
    code character varying(255),
    description text,
    notes text,
    definition_id integer,
    defs_option_id integer,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.opts_codes OWNER TO postgres;

--
-- Name: opts_codes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opts_codes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opts_codes_id_seq OWNER TO postgres;

--
-- Name: opts_codes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.opts_codes_id_seq OWNED BY public.opts_codes.id;


--
-- Name: opts_sentences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.opts_sentences (
    id bigint NOT NULL,
    language character varying(255),
    sentence text,
    note text,
    definition_id integer,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    defs_option_id bigint
);


ALTER TABLE public.opts_sentences OWNER TO postgres;

--
-- Name: opts_sentences_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.opts_sentences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.opts_sentences_id_seq OWNER TO postgres;

--
-- Name: opts_sentences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.opts_sentences_id_seq OWNED BY public.opts_sentences.id;


--
-- Name: patients; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.patients (
    id bigint NOT NULL,
    patnts_last_name character varying(50),
    patnts_first_name character varying(50),
    patnts_gender character varying(5),
    patnts_middle_name character varying(50),
    patnts_father_name character varying(50),
    patnts_mother_name character varying(50),
    patnts_birth_dt date,
    patient_other_id character varying(255),
    patnts_ssn character varying(15),
    patnts_ssn_na boolean DEFAULT false NOT NULL,
    patnts_deceased boolean DEFAULT false NOT NULL,
    patnts_record_active boolean DEFAULT false NOT NULL,
    patnts_language_preferred character varying(12),
    patnts_race character varying(50),
    patnts_uuid uuid,
    patnts_notes text,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL,
    patnts_ethnicity_hispanic boolean DEFAULT false NOT NULL
);


ALTER TABLE public.patients OWNER TO postgres;

--
-- Name: patients_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.patients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patients_id_seq OWNER TO postgres;

--
-- Name: patients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.patients_id_seq OWNED BY public.patients.id;


--
-- Name: proc_episodes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proc_episodes (
    id bigint NOT NULL,
    proc_epis_request_dt date,
    proc_epis_request_tm time(0) without time zone,
    proc_epis_cor_machine character varying(50),
    proc_epis_cor_machine_loc character varying(50),
    proc_epis_cor_machine_id character varying(50),
    proc_epis_refer_person character varying(50),
    proc_epis_prim_operetor_fname character varying(50),
    proc_epis_prim_operator_lname character varying(50),
    proc_epis_prim_operator_mname character varying(50),
    proc_epis_prim_operator_npi character varying(50),
    proc_epis_art_access_r_femoral character varying(50),
    proc_epis_art_access_l_femoral character varying(50),
    proc_epis_art_access_r_brachial character varying(50),
    proc_epis_art_access_l_brachial character varying(50),
    caepsds_art_access_r_radial character varying(50),
    proc_epis_art_access_l_radial character varying(50),
    proc_epis_closure_device character varying(50),
    proc_epis_icd10_pcs_1 character varying(50),
    proc_epis_icd10_pcs_2 character varying(50),
    proc_epis_icd10_pcs_3 character varying(50),
    proc_epis_icd10_pcs_4 character varying(50),
    proc_epis_contrast_volume character varying(50),
    proc_epis_contrast_name character varying(50),
    proc_epis_catheter_l_name character varying(50),
    proc_epis_catheter_l_size character varying(50),
    proc_epis_catheter_r_name character varying(50),
    proc_epis_catheter_r_size character varying(50),
    proc_epis_catheter_lv_name character varying(50),
    proc_epis_catheter_lv_size character varying(50),
    proc_epis_notes text,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.proc_episodes OWNER TO postgres;

--
-- Name: proc_episodes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proc_episodes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proc_episodes_id_seq OWNER TO postgres;

--
-- Name: proc_episodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proc_episodes_id_seq OWNED BY public.proc_episodes.id;


--
-- Name: sats_pressures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sats_pressures (
    id bigint NOT NULL,
    aortic_pres_mean character varying(255),
    pa_right_pres_mean character varying(255),
    pa_pres_sys character varying(255),
    rv_pres_dias character varying(255),
    svc_pres_dias character varying(255),
    ivc_pres_mean character varying(255),
    svc_pres_mean character varying(255),
    sao2_rv character varying(255),
    sao2_ra_mid character varying(255),
    ra_pres_x character varying(255),
    rv_pres_sys character varying(255),
    pa_left_pres_sys character varying(255),
    sao2_hepatic_veins character varying(255),
    sao2_rv_apex character varying(255),
    sat_pres_meds character varying(255),
    pcw_pres_dias character varying(255),
    sao2_svc_low character varying(255),
    sao2_pa character varying(255),
    sat_pres_o2_inhalation character varying(255),
    lv_pres_sys character varying(255),
    ra_pres_v character varying(255),
    pcw_pres_x character varying(255),
    pa_right_pres_sys character varying(255),
    pa_right_pres_dias character varying(255),
    ventr_dias_fill_period character varying(255),
    pcw_pres_h character varying(255),
    pcw_pres_a character varying(255),
    sao2_pa_left character varying(255),
    pcw_pres_z character varying(255),
    ra_pres_mean character varying(255),
    pa_pres_mean character varying(255),
    sao2_rv_mid character varying(255),
    pa_left_pres_mean character varying(255),
    sao2_lv character varying(255),
    pcw_pres_v character varying(255),
    aortic_pres_dias character varying(255),
    svc_pres_sys character varying(255),
    ventr_sys_ejc_period character varying(255),
    sao2_ivc character varying(255),
    sat_pres_hb character varying(255),
    ivc_pres_sys character varying(255),
    pcw_pres_c character varying(255),
    aortic_pres_sys character varying(255),
    uni_bsa character varying(255),
    pcw_pres_mean character varying(255),
    ra_pres_h character varying(255),
    pa_pres_sys_na character varying(255),
    ra_pres_dias character varying(255),
    sao2_rv_outflow character varying(255),
    ra_pres_a character varying(255),
    pa_left_pres_dias character varying(255),
    ra_pres_z character varying(255),
    uni_qp_qs character varying(255),
    pa_pres_mean_na character varying(255),
    sat_pres_oxygen_consumption character varying(255),
    lv_pres_mean character varying(255),
    sao2_ao_distal character varying(255),
    sao2_pcw character varying(255),
    ivc_pres_dias character varying(255),
    lv_pres_end_dias character varying(255),
    pa_pres_dias character varying(255),
    sao2_ra_low character varying(255),
    sat_pres_notes character varying(255),
    ra_pres_c character varying(255),
    ra_pres_sys character varying(255),
    sao2_ra_high character varying(255),
    sao2_svc_high character varying(255),
    rv_pres_mean character varying(255),
    sao2_pa_right character varying(255),
    sao2_ao_root character varying(255),
    pcw_pres_y character varying(255),
    pcw_pres_sys character varying(255),
    sat_pulm_veins character varying(255),
    sat_pres_start_dt date,
    sat_pres_start_tm time(0) without time zone,
    sat_pres_end_dt date,
    sat_pres_end_tm time(0) without time zone,
    ra_pres_y character varying(255),
    sat_pres_heart_rate character varying(255),
    hepatic_veins_pres character varying(255),
    patient_id integer,
    info_coronary_id bigint,
    inserted_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.sats_pressures OWNER TO postgres;

--
-- Name: sats_pressures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sats_pressures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sats_pressures_id_seq OWNER TO postgres;

--
-- Name: sats_pressures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sats_pressures_id_seq OWNED BY public.sats_pressures.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    inserted_at timestamp(0) without time zone
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: cath_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_events ALTER COLUMN id SET DEFAULT nextval('public.cath_events_id_seq'::regclass);


--
-- Name: cath_graft id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_graft ALTER COLUMN id SET DEFAULT nextval('public.cath_graft_id_seq'::regclass);


--
-- Name: cath_grafts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_grafts ALTER COLUMN id SET DEFAULT nextval('public.cath_grafts_id_seq'::regclass);


--
-- Name: cath_meds id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_meds ALTER COLUMN id SET DEFAULT nextval('public.cath_meds_id_seq'::regclass);


--
-- Name: cath_radiations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_radiations ALTER COLUMN id SET DEFAULT nextval('public.cath_radiations_id_seq'::regclass);


--
-- Name: cor_anatomies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_anatomies ALTER COLUMN id SET DEFAULT nextval('public.cor_anatomies_id_seq'::regclass);


--
-- Name: cor_collaterals id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_collaterals ALTER COLUMN id SET DEFAULT nextval('public.cor_collaterals_id_seq'::regclass);


--
-- Name: cor_lesions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_lesions ALTER COLUMN id SET DEFAULT nextval('public.cor_lesions_id_seq'::regclass);


--
-- Name: countries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);


--
-- Name: definitions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.definitions ALTER COLUMN id SET DEFAULT nextval('public.definitions_id_seq'::regclass);


--
-- Name: defs_codes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_codes ALTER COLUMN id SET DEFAULT nextval('public.defs_codes_id_seq'::regclass);


--
-- Name: defs_options id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_options ALTER COLUMN id SET DEFAULT nextval('public.defs_options_id_seq'::regclass);


--
-- Name: defs_sentences id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_sentences ALTER COLUMN id SET DEFAULT nextval('public.defs_sentences_id_seq'::regclass);


--
-- Name: defs_tables id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_tables ALTER COLUMN id SET DEFAULT nextval('public.defs_tables_id_seq'::regclass);


--
-- Name: info_coronaries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info_coronaries ALTER COLUMN id SET DEFAULT nextval('public.info_coronaries_id_seq'::regclass);


--
-- Name: languages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.languages ALTER COLUMN id SET DEFAULT nextval('public.languages_id_seq'::regclass);


--
-- Name: lventricles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lventricles ALTER COLUMN id SET DEFAULT nextval('public.lventricles_id_seq'::regclass);


--
-- Name: opts_codes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opts_codes ALTER COLUMN id SET DEFAULT nextval('public.opts_codes_id_seq'::regclass);


--
-- Name: opts_sentences id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opts_sentences ALTER COLUMN id SET DEFAULT nextval('public.opts_sentences_id_seq'::regclass);


--
-- Name: patients id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.patients ALTER COLUMN id SET DEFAULT nextval('public.patients_id_seq'::regclass);


--
-- Name: proc_episodes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proc_episodes ALTER COLUMN id SET DEFAULT nextval('public.proc_episodes_id_seq'::regclass);


--
-- Name: sats_pressures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sats_pressures ALTER COLUMN id SET DEFAULT nextval('public.sats_pressures_id_seq'::regclass);


--
-- Name: cath_events cath_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_events
    ADD CONSTRAINT cath_events_pkey PRIMARY KEY (id);


--
-- Name: cath_graft cath_graft_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_graft
    ADD CONSTRAINT cath_graft_pkey PRIMARY KEY (id);


--
-- Name: cath_grafts cath_grafts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_grafts
    ADD CONSTRAINT cath_grafts_pkey PRIMARY KEY (id);


--
-- Name: cath_meds cath_meds_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_meds
    ADD CONSTRAINT cath_meds_pkey PRIMARY KEY (id);


--
-- Name: cath_radiations cath_radiations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_radiations
    ADD CONSTRAINT cath_radiations_pkey PRIMARY KEY (id);


--
-- Name: cor_anatomies cor_anatomies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_anatomies
    ADD CONSTRAINT cor_anatomies_pkey PRIMARY KEY (id);


--
-- Name: cor_collaterals cor_collaterals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_collaterals
    ADD CONSTRAINT cor_collaterals_pkey PRIMARY KEY (id);


--
-- Name: cor_lesions cor_lesions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_lesions
    ADD CONSTRAINT cor_lesions_pkey PRIMARY KEY (id);


--
-- Name: countries countries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: definitions definitions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.definitions
    ADD CONSTRAINT definitions_pkey PRIMARY KEY (id);


--
-- Name: defs_codes defs_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_codes
    ADD CONSTRAINT defs_codes_pkey PRIMARY KEY (id);


--
-- Name: defs_options defs_options_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_options
    ADD CONSTRAINT defs_options_pkey PRIMARY KEY (id);


--
-- Name: defs_sentences defs_sentences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_sentences
    ADD CONSTRAINT defs_sentences_pkey PRIMARY KEY (id);


--
-- Name: defs_tables defs_tables_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_tables
    ADD CONSTRAINT defs_tables_pkey PRIMARY KEY (id);


--
-- Name: info_coronaries info_coronaries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info_coronaries
    ADD CONSTRAINT info_coronaries_pkey PRIMARY KEY (id);


--
-- Name: languages languages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: lventricles lventricles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lventricles
    ADD CONSTRAINT lventricles_pkey PRIMARY KEY (id);


--
-- Name: opts_codes opts_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opts_codes
    ADD CONSTRAINT opts_codes_pkey PRIMARY KEY (id);


--
-- Name: opts_sentences opts_sentences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opts_sentences
    ADD CONSTRAINT opts_sentences_pkey PRIMARY KEY (id);


--
-- Name: patients patients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.patients
    ADD CONSTRAINT patients_pkey PRIMARY KEY (id);


--
-- Name: proc_episodes proc_episodes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proc_episodes
    ADD CONSTRAINT proc_episodes_pkey PRIMARY KEY (id);


--
-- Name: sats_pressures sats_pressures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sats_pressures
    ADD CONSTRAINT sats_pressures_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: definitions_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX definitions_name_index ON public.definitions USING btree (name);


--
-- Name: patients_patnts_last_name_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX patients_patnts_last_name_index ON public.patients USING btree (patnts_last_name);


--
-- Name: cath_events cath_events_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_events
    ADD CONSTRAINT cath_events_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: cath_grafts cath_grafts_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_grafts
    ADD CONSTRAINT cath_grafts_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: cath_meds cath_meds_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_meds
    ADD CONSTRAINT cath_meds_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: cath_radiations cath_radiations_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cath_radiations
    ADD CONSTRAINT cath_radiations_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: cor_anatomies cor_anatomies_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_anatomies
    ADD CONSTRAINT cor_anatomies_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: cor_collaterals cor_collaterals_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_collaterals
    ADD CONSTRAINT cor_collaterals_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: cor_lesions cor_lesions_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cor_lesions
    ADD CONSTRAINT cor_lesions_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: defs_codes defs_codes_definition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_codes
    ADD CONSTRAINT defs_codes_definition_id_fkey FOREIGN KEY (definition_id) REFERENCES public.definitions(id) ON DELETE CASCADE;


--
-- Name: defs_options defs_options_definition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_options
    ADD CONSTRAINT defs_options_definition_id_fkey FOREIGN KEY (definition_id) REFERENCES public.definitions(id) ON DELETE CASCADE;


--
-- Name: defs_sentences defs_sentences_definition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_sentences
    ADD CONSTRAINT defs_sentences_definition_id_fkey FOREIGN KEY (definition_id) REFERENCES public.definitions(id) ON DELETE CASCADE;


--
-- Name: defs_tables defs_tables_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.defs_tables
    ADD CONSTRAINT defs_tables_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: info_coronaries info_coronaries_patient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.info_coronaries
    ADD CONSTRAINT info_coronaries_patient_id_fkey FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON DELETE CASCADE;


--
-- Name: lventricles lventricles_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lventricles
    ADD CONSTRAINT lventricles_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: opts_sentences opts_sentences_defs_option_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.opts_sentences
    ADD CONSTRAINT opts_sentences_defs_option_id_fkey FOREIGN KEY (defs_option_id) REFERENCES public.defs_options(id) ON DELETE CASCADE;


--
-- Name: proc_episodes proc_episodes_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proc_episodes
    ADD CONSTRAINT proc_episodes_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- Name: sats_pressures sats_pressures_info_coronary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sats_pressures
    ADD CONSTRAINT sats_pressures_info_coronary_id_fkey FOREIGN KEY (info_coronary_id) REFERENCES public.info_coronaries(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

