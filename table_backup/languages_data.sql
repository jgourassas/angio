--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.9 (Ubuntu 10.9-0ubuntu0.18.04.1)

-- Started on 2019-10-23 07:13:59 EEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3014 (class 0 OID 98695)
-- Dependencies: 201
-- Data for Name: languages; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.languages (id, english, alpha2) VALUES (1, 'Afar', 'aa');
INSERT INTO public.languages (id, english, alpha2) VALUES (2, 'Abkhazian', 'ab');
INSERT INTO public.languages (id, english, alpha2) VALUES (3, 'Avestan', 'ae');
INSERT INTO public.languages (id, english, alpha2) VALUES (4, 'Afrikaans', 'af');
INSERT INTO public.languages (id, english, alpha2) VALUES (5, 'Akan', 'ak');
INSERT INTO public.languages (id, english, alpha2) VALUES (6, 'Amharic', 'am');
INSERT INTO public.languages (id, english, alpha2) VALUES (7, 'Aragonese', 'an');
INSERT INTO public.languages (id, english, alpha2) VALUES (8, 'Arabic', 'ar');
INSERT INTO public.languages (id, english, alpha2) VALUES (9, 'Assamese', 'as');
INSERT INTO public.languages (id, english, alpha2) VALUES (10, 'Avaric', 'av');
INSERT INTO public.languages (id, english, alpha2) VALUES (11, 'Aymara', 'ay');
INSERT INTO public.languages (id, english, alpha2) VALUES (12, 'Azerbaijani', 'az');
INSERT INTO public.languages (id, english, alpha2) VALUES (13, 'Bashkir', 'ba');
INSERT INTO public.languages (id, english, alpha2) VALUES (14, 'Belarusian', 'be');
INSERT INTO public.languages (id, english, alpha2) VALUES (15, 'Bulgarian', 'bg');
INSERT INTO public.languages (id, english, alpha2) VALUES (16, 'Bihari languages', 'bh');
INSERT INTO public.languages (id, english, alpha2) VALUES (17, 'Bislama', 'bi');
INSERT INTO public.languages (id, english, alpha2) VALUES (18, 'Bambara', 'bm');
INSERT INTO public.languages (id, english, alpha2) VALUES (19, 'Bengali', 'bn');
INSERT INTO public.languages (id, english, alpha2) VALUES (20, 'Tibetan', 'bo');
INSERT INTO public.languages (id, english, alpha2) VALUES (21, 'Breton', 'br');
INSERT INTO public.languages (id, english, alpha2) VALUES (22, 'Bosnian', 'bs');
INSERT INTO public.languages (id, english, alpha2) VALUES (23, 'Catalan; Valencian', 'ca');
INSERT INTO public.languages (id, english, alpha2) VALUES (24, 'Chechen', 'ce');
INSERT INTO public.languages (id, english, alpha2) VALUES (25, 'Chamorro', 'ch');
INSERT INTO public.languages (id, english, alpha2) VALUES (26, 'Corsican', 'co');
INSERT INTO public.languages (id, english, alpha2) VALUES (27, 'Cree', 'cr');
INSERT INTO public.languages (id, english, alpha2) VALUES (28, 'Czech', 'cs');
INSERT INTO public.languages (id, english, alpha2) VALUES (29, 'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic', 'cu');
INSERT INTO public.languages (id, english, alpha2) VALUES (30, 'Chuvash', 'cv');
INSERT INTO public.languages (id, english, alpha2) VALUES (31, 'Welsh', 'cy');
INSERT INTO public.languages (id, english, alpha2) VALUES (32, 'Danish', 'da');
INSERT INTO public.languages (id, english, alpha2) VALUES (33, 'German', 'de');
INSERT INTO public.languages (id, english, alpha2) VALUES (34, 'Divehi; Dhivehi; Maldivian', 'dv');
INSERT INTO public.languages (id, english, alpha2) VALUES (35, 'Dzongkha', 'dz');
INSERT INTO public.languages (id, english, alpha2) VALUES (36, 'Ewe', 'ee');
INSERT INTO public.languages (id, english, alpha2) VALUES (37, 'Greek, Modern (1453-)', 'el');
INSERT INTO public.languages (id, english, alpha2) VALUES (38, 'English', 'en');
INSERT INTO public.languages (id, english, alpha2) VALUES (39, 'Esperanto', 'eo');
INSERT INTO public.languages (id, english, alpha2) VALUES (40, 'Spanish; Castilian', 'es');
INSERT INTO public.languages (id, english, alpha2) VALUES (41, 'Estonian', 'et');
INSERT INTO public.languages (id, english, alpha2) VALUES (42, 'Basque', 'eu');
INSERT INTO public.languages (id, english, alpha2) VALUES (43, 'Persian', 'fa');
INSERT INTO public.languages (id, english, alpha2) VALUES (44, 'Fulah', 'ff');
INSERT INTO public.languages (id, english, alpha2) VALUES (45, 'Finnish', 'fi');
INSERT INTO public.languages (id, english, alpha2) VALUES (46, 'Fijian', 'fj');
INSERT INTO public.languages (id, english, alpha2) VALUES (47, 'Faroese', 'fo');
INSERT INTO public.languages (id, english, alpha2) VALUES (48, 'French', 'fr');
INSERT INTO public.languages (id, english, alpha2) VALUES (49, 'Western Frisian', 'fy');
INSERT INTO public.languages (id, english, alpha2) VALUES (50, 'Irish', 'ga');
INSERT INTO public.languages (id, english, alpha2) VALUES (51, 'Gaelic; Scottish Gaelic', 'gd');
INSERT INTO public.languages (id, english, alpha2) VALUES (52, 'Galician', 'gl');
INSERT INTO public.languages (id, english, alpha2) VALUES (53, 'Guarani', 'gn');
INSERT INTO public.languages (id, english, alpha2) VALUES (54, 'Gujarati', 'gu');
INSERT INTO public.languages (id, english, alpha2) VALUES (55, 'Manx', 'gv');
INSERT INTO public.languages (id, english, alpha2) VALUES (56, 'Hausa', 'ha');
INSERT INTO public.languages (id, english, alpha2) VALUES (57, 'Hebrew', 'he');
INSERT INTO public.languages (id, english, alpha2) VALUES (58, 'Hindi', 'hi');
INSERT INTO public.languages (id, english, alpha2) VALUES (59, 'Hiri Motu', 'ho');
INSERT INTO public.languages (id, english, alpha2) VALUES (60, 'Croatian', 'hr');
INSERT INTO public.languages (id, english, alpha2) VALUES (61, 'Haitian; Haitian Creole', 'ht');
INSERT INTO public.languages (id, english, alpha2) VALUES (62, 'Hungarian', 'hu');
INSERT INTO public.languages (id, english, alpha2) VALUES (63, 'Armenian', 'hy');
INSERT INTO public.languages (id, english, alpha2) VALUES (64, 'Herero', 'hz');
INSERT INTO public.languages (id, english, alpha2) VALUES (65, 'Interlingua (International Auxiliary Language Association)', 'ia');
INSERT INTO public.languages (id, english, alpha2) VALUES (66, 'Indonesian', 'id');
INSERT INTO public.languages (id, english, alpha2) VALUES (67, 'Interlingue; Occidental', 'ie');
INSERT INTO public.languages (id, english, alpha2) VALUES (68, 'Igbo', 'ig');
INSERT INTO public.languages (id, english, alpha2) VALUES (69, 'Sichuan Yi; Nuosu', 'ii');
INSERT INTO public.languages (id, english, alpha2) VALUES (70, 'Inupiaq', 'ik');
INSERT INTO public.languages (id, english, alpha2) VALUES (71, 'Ido', 'io');
INSERT INTO public.languages (id, english, alpha2) VALUES (72, 'Icelandic', 'is');
INSERT INTO public.languages (id, english, alpha2) VALUES (73, 'Italian', 'it');
INSERT INTO public.languages (id, english, alpha2) VALUES (74, 'Inuktitut', 'iu');
INSERT INTO public.languages (id, english, alpha2) VALUES (75, 'Japanese', 'ja');
INSERT INTO public.languages (id, english, alpha2) VALUES (76, 'Javanese', 'jv');
INSERT INTO public.languages (id, english, alpha2) VALUES (77, 'Georgian', 'ka');
INSERT INTO public.languages (id, english, alpha2) VALUES (78, 'Kongo', 'kg');
INSERT INTO public.languages (id, english, alpha2) VALUES (79, 'Kikuyu; Gikuyu', 'ki');
INSERT INTO public.languages (id, english, alpha2) VALUES (80, 'Kuanyama; Kwanyama', 'kj');
INSERT INTO public.languages (id, english, alpha2) VALUES (81, 'Kazakh', 'kk');
INSERT INTO public.languages (id, english, alpha2) VALUES (82, 'Kalaallisut; Greenlandic', 'kl');
INSERT INTO public.languages (id, english, alpha2) VALUES (83, 'Central Khmer', 'km');
INSERT INTO public.languages (id, english, alpha2) VALUES (84, 'Kannada', 'kn');
INSERT INTO public.languages (id, english, alpha2) VALUES (85, 'Korean', 'ko');
INSERT INTO public.languages (id, english, alpha2) VALUES (86, 'Kanuri', 'kr');
INSERT INTO public.languages (id, english, alpha2) VALUES (87, 'Kashmiri', 'ks');
INSERT INTO public.languages (id, english, alpha2) VALUES (88, 'Kurdish', 'ku');
INSERT INTO public.languages (id, english, alpha2) VALUES (89, 'Komi', 'kv');
INSERT INTO public.languages (id, english, alpha2) VALUES (90, 'Cornish', 'kw');
INSERT INTO public.languages (id, english, alpha2) VALUES (91, 'Kirghiz; Kyrgyz', 'ky');
INSERT INTO public.languages (id, english, alpha2) VALUES (92, 'Latin', 'la');
INSERT INTO public.languages (id, english, alpha2) VALUES (93, 'Luxembourgish; Letzeburgesch', 'lb');
INSERT INTO public.languages (id, english, alpha2) VALUES (94, 'Ganda', 'lg');
INSERT INTO public.languages (id, english, alpha2) VALUES (95, 'Limburgan; Limburger; Limburgish', 'li');
INSERT INTO public.languages (id, english, alpha2) VALUES (96, 'Lingala', 'ln');
INSERT INTO public.languages (id, english, alpha2) VALUES (97, 'Lao', 'lo');
INSERT INTO public.languages (id, english, alpha2) VALUES (98, 'Lithuanian', 'lt');
INSERT INTO public.languages (id, english, alpha2) VALUES (99, 'Luba-Katanga', 'lu');
INSERT INTO public.languages (id, english, alpha2) VALUES (100, 'Latvian', 'lv');
INSERT INTO public.languages (id, english, alpha2) VALUES (101, 'Malagasy', 'mg');
INSERT INTO public.languages (id, english, alpha2) VALUES (102, 'Marshallese', 'mh');
INSERT INTO public.languages (id, english, alpha2) VALUES (103, 'Maori', 'mi');
INSERT INTO public.languages (id, english, alpha2) VALUES (104, 'Macedonian', 'mk');
INSERT INTO public.languages (id, english, alpha2) VALUES (105, 'Malayalam', 'ml');
INSERT INTO public.languages (id, english, alpha2) VALUES (106, 'Mongolian', 'mn');
INSERT INTO public.languages (id, english, alpha2) VALUES (107, 'Marathi', 'mr');
INSERT INTO public.languages (id, english, alpha2) VALUES (108, 'Malay', 'ms');
INSERT INTO public.languages (id, english, alpha2) VALUES (109, 'Maltese', 'mt');
INSERT INTO public.languages (id, english, alpha2) VALUES (110, 'Burmese', 'my');
INSERT INTO public.languages (id, english, alpha2) VALUES (111, 'Nauru', 'na');
INSERT INTO public.languages (id, english, alpha2) VALUES (112, 'Bokmål, Norwegian; Norwegian Bokmål', 'nb');
INSERT INTO public.languages (id, english, alpha2) VALUES (113, 'Ndebele, North; North Ndebele', 'nd');
INSERT INTO public.languages (id, english, alpha2) VALUES (114, 'Nepali', 'ne');
INSERT INTO public.languages (id, english, alpha2) VALUES (115, 'Ndonga', 'ng');
INSERT INTO public.languages (id, english, alpha2) VALUES (116, 'Dutch; Flemish', 'nl');
INSERT INTO public.languages (id, english, alpha2) VALUES (117, 'Norwegian Nynorsk; Nynorsk, Norwegian', 'nn');
INSERT INTO public.languages (id, english, alpha2) VALUES (118, 'Norwegian', 'no');
INSERT INTO public.languages (id, english, alpha2) VALUES (119, 'Ndebele, South; South Ndebele', 'nr');
INSERT INTO public.languages (id, english, alpha2) VALUES (120, 'Navajo; Navaho', 'nv');
INSERT INTO public.languages (id, english, alpha2) VALUES (121, 'Chichewa; Chewa; Nyanja', 'ny');
INSERT INTO public.languages (id, english, alpha2) VALUES (122, 'Occitan (post 1500); Provençal', 'oc');
INSERT INTO public.languages (id, english, alpha2) VALUES (123, 'Ojibwa', 'oj');
INSERT INTO public.languages (id, english, alpha2) VALUES (124, 'Oromo', 'om');
INSERT INTO public.languages (id, english, alpha2) VALUES (125, 'Oriya', 'or');
INSERT INTO public.languages (id, english, alpha2) VALUES (126, 'Ossetian; Ossetic', 'os');
INSERT INTO public.languages (id, english, alpha2) VALUES (127, 'Panjabi; Punjabi', 'pa');
INSERT INTO public.languages (id, english, alpha2) VALUES (128, 'Pali', 'pi');
INSERT INTO public.languages (id, english, alpha2) VALUES (129, 'Polish', 'pl');
INSERT INTO public.languages (id, english, alpha2) VALUES (130, 'Pushto; Pashto', 'ps');
INSERT INTO public.languages (id, english, alpha2) VALUES (131, 'Portuguese', 'pt');
INSERT INTO public.languages (id, english, alpha2) VALUES (132, 'Quechua', 'qu');
INSERT INTO public.languages (id, english, alpha2) VALUES (133, 'Romansh', 'rm');
INSERT INTO public.languages (id, english, alpha2) VALUES (134, 'Rundi', 'rn');
INSERT INTO public.languages (id, english, alpha2) VALUES (135, 'Romanian; Moldavian; Moldovan', 'ro');
INSERT INTO public.languages (id, english, alpha2) VALUES (136, 'Russian', 'ru');
INSERT INTO public.languages (id, english, alpha2) VALUES (137, 'Kinyarwanda', 'rw');
INSERT INTO public.languages (id, english, alpha2) VALUES (138, 'Sanskrit', 'sa');
INSERT INTO public.languages (id, english, alpha2) VALUES (139, 'Sardinian', 'sc');
INSERT INTO public.languages (id, english, alpha2) VALUES (140, 'Sindhi', 'sd');
INSERT INTO public.languages (id, english, alpha2) VALUES (141, 'Northern Sami', 'se');
INSERT INTO public.languages (id, english, alpha2) VALUES (142, 'Sango', 'sg');
INSERT INTO public.languages (id, english, alpha2) VALUES (143, 'Sinhala; Sinhalese', 'si');
INSERT INTO public.languages (id, english, alpha2) VALUES (144, 'Slovak', 'sk');
INSERT INTO public.languages (id, english, alpha2) VALUES (145, 'Slovenian', 'sl');
INSERT INTO public.languages (id, english, alpha2) VALUES (146, 'Samoan', 'sm');
INSERT INTO public.languages (id, english, alpha2) VALUES (147, 'Shona', 'sn');
INSERT INTO public.languages (id, english, alpha2) VALUES (148, 'Somali', 'so');
INSERT INTO public.languages (id, english, alpha2) VALUES (149, 'Albanian', 'sq');
INSERT INTO public.languages (id, english, alpha2) VALUES (150, 'Serbian', 'sr');
INSERT INTO public.languages (id, english, alpha2) VALUES (151, 'Swati', 'ss');
INSERT INTO public.languages (id, english, alpha2) VALUES (152, 'Sotho, Southern', 'st');
INSERT INTO public.languages (id, english, alpha2) VALUES (153, 'Sundanese', 'su');
INSERT INTO public.languages (id, english, alpha2) VALUES (154, 'Swedish', 'sv');
INSERT INTO public.languages (id, english, alpha2) VALUES (155, 'Swahili', 'sw');
INSERT INTO public.languages (id, english, alpha2) VALUES (156, 'Tamil', 'ta');
INSERT INTO public.languages (id, english, alpha2) VALUES (157, 'Telugu', 'te');
INSERT INTO public.languages (id, english, alpha2) VALUES (158, 'Tajik', 'tg');
INSERT INTO public.languages (id, english, alpha2) VALUES (159, 'Thai', 'th');
INSERT INTO public.languages (id, english, alpha2) VALUES (160, 'Tigrinya', 'ti');
INSERT INTO public.languages (id, english, alpha2) VALUES (161, 'Turkmen', 'tk');
INSERT INTO public.languages (id, english, alpha2) VALUES (162, 'Tagalog', 'tl');
INSERT INTO public.languages (id, english, alpha2) VALUES (163, 'Tswana', 'tn');
INSERT INTO public.languages (id, english, alpha2) VALUES (164, 'Tonga (Tonga Islands)', 'to');
INSERT INTO public.languages (id, english, alpha2) VALUES (165, 'Turkish', 'tr');
INSERT INTO public.languages (id, english, alpha2) VALUES (166, 'Tsonga', 'ts');
INSERT INTO public.languages (id, english, alpha2) VALUES (167, 'Tatar', 'tt');
INSERT INTO public.languages (id, english, alpha2) VALUES (168, 'Twi', 'tw');
INSERT INTO public.languages (id, english, alpha2) VALUES (169, 'Tahitian', 'ty');
INSERT INTO public.languages (id, english, alpha2) VALUES (170, 'Uighur; Uyghur', 'ug');
INSERT INTO public.languages (id, english, alpha2) VALUES (171, 'Ukrainian', 'uk');
INSERT INTO public.languages (id, english, alpha2) VALUES (172, 'Urdu', 'ur');
INSERT INTO public.languages (id, english, alpha2) VALUES (173, 'Uzbek', 'uz');
INSERT INTO public.languages (id, english, alpha2) VALUES (174, 'Venda', 've');
INSERT INTO public.languages (id, english, alpha2) VALUES (175, 'Vietnamese', 'vi');
INSERT INTO public.languages (id, english, alpha2) VALUES (176, 'Volapük', 'vo');
INSERT INTO public.languages (id, english, alpha2) VALUES (177, 'Walloon', 'wa');
INSERT INTO public.languages (id, english, alpha2) VALUES (178, 'Wolof', 'wo');
INSERT INTO public.languages (id, english, alpha2) VALUES (179, 'Xhosa', 'xh');
INSERT INTO public.languages (id, english, alpha2) VALUES (180, 'Yiddish', 'yi');
INSERT INTO public.languages (id, english, alpha2) VALUES (181, 'Yoruba', 'yo');
INSERT INTO public.languages (id, english, alpha2) VALUES (182, 'Zhuang; Chuang', 'za');
INSERT INTO public.languages (id, english, alpha2) VALUES (183, 'Chinese', 'zh');
INSERT INTO public.languages (id, english, alpha2) VALUES (184, 'Zulu', 'zu');


--
-- TOC entry 3020 (class 0 OID 0)
-- Dependencies: 200
-- Name: languages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.languages_id_seq', 184, true);


-- Completed on 2019-10-23 07:13:59 EEST

--
-- PostgreSQL database dump complete
--

