
## Angio Is a  Archiving  system for  Cath Lab


**It Records:**

   * Patient Demographics
   * Procedure Details: Request Dates, ICDcm-Codes, ICD10-PCS, Catheters Used...
   * Coronary Anatomy:  Normal Arteries and Most Frequent Vessels Variations
   * The Lesions
   * The Collaterals
   * Grafts, T-Grafts: Positions, Lesions
   * Left Ventricle: Lv segments wall motion
   * Cath Events: Death, Infarction, Shock, Vascular Complications...
   * Radiation Doses: Fluoro Time, Kerma...
   * Medications
 
 **Can Display,  from the saved data, as a grotesco Sketch (d3.js)**

   * The Coronary Arteries according to the Dominance
   * Anatomy - Frequent  Coronary Variants or Anomalies
   * The Grafts inserted and  the T-Grafts 
   * The Lesions in each segment of Coronaries (Segmentation Model CDISC)
   * The Collaterals
   * Bulls eye (17 segnent Model)
 
 **A narrative Summary Report can be produced**



 **Utility Tables**

   * Fields (~ 3000 entries). Contains Field Name, Coding Instructions, Range 
      and some  extensive and important Notes About the entity
   * Pick-List Items 
   * Field Sentences. Sentences In Defferent Languages - Add your own - To be used in the Report
   * Field Codes can be saved as: SNOMED-CT - Loinc - RxNorm - HL7 - RadLex
   * Pick-List Items Sentences 
   * Pick-List Items Codes.



### Please Read and the READMY_ANGIO.txt




## To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * As postgres user insert all .sql in the angio_dev database from the dir table_backup
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
  
