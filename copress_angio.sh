#!/bin/sh
DATE=`date +%Y%m%d`
echo " --- Please run first git at master "
git archive --format zip --output ../angio.zip   --prefix=angio/ master
echo "--- I have finished ziping "
git archive --format tar --output ../angio.tar  --prefix=angio/ master 
echo "--- I have finished gzip"
cd ../
rm angio-*.tar.gz
rm angio-*.zip
rm angio_tables_backup-*


gzip -9 angio.tar

mv angio.tar.gz angio-$DATE.tar.gz
mv angio.zip    angio-$DATE.zip

cd angio/tables_backup/

./backup_dev_data.sh

echo "=================="
echo "END  Check ../ You have the files  angio-DATE.tar.gz and angio in zip"
echo "=================="




