 <!---------start----------------------------------->
  <div class = "columns">
   <span class = "tag is-info"> A tag</span>
<div class = "column">
11111111
</div><!----column-------------------->
 <div class = "column">
 2222222
 </div><!----column-------------------->

<div class = "column">
 3333333333
 </div><!----column-------------------->


</div><!----columns--------------------------->

<!---------end start------------------------------------>

check version

mix phoenix.new -v
Phoenix v1.3.0

elixir -v
Erlang/OTP 21 [DEVELOPMENT] [erts-9.0] 
[source] [64-bit] [smp:4:4] [ds:4:4:10] 
[async-threads:10] [hipe] [kernel-poll:false]

Elixir 1.7.0-dev (1b8b1ed9b) (compiled with OTP 19)

mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez

implantable devices
https://accessgudid.nlm.nih.gov/download

https://accessgudid.nlm.nih.gov/download/delimited //121MB
------------------
In http:/localhost:4000/graphiql
for Report
check this: @absinthe/socket
 npm install --save @absinthe/socket
 <!--=Migration Example========================-->
$ mix ecto.gen.migration update_posts_table
  creating priv/repo/migrations/20160602085927_update_posts_table.exs
  ···
$ mix ecto.migrate
$ mix ecto.rollback

defmodule MyRepo.Migrations.AddWeatherTable do
        use Ecto.Migration
        def up do
          create table("weather") do
            add :city,    :string, size: 40
            add :temp_lo, :integer
            add :temp_hi, :integer
            add :prcp,    :float
            timestamps()
          end
        end
        def down do
          drop table("weather")
        end
      end
 
	  ------OR--------------
 defmodule MyRepo.Migrations.AddWeatherTable do
        use Ecto.Migration
        def change do
          create table("weather") do
            add :city,    :string, size: 40
            add :temp_lo, :integer
            add :temp_hi, :integer
            add :prcp,    :float
            timestamps()
          end
        end

      end	

	  ------OR----WITH PREFIX----------

	    def up do
        create table("weather", prefix: "north_america") do
          add :city,    :string, size: 40
          add :temp_lo, :integer
          add :temp_hi, :integer
          add :prcp,    :float
          add :group_id, references(:groups)
          timestamps()
        end
        create index("weather", [:city], prefix: "north_america")
      end

defmodule MyApp.Repo.Migrations.CreateReading do
  use Ecto.Migration

  def change do
    create table(:readings, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :raw_data, :map
      add :timestamp, :float

      timestamps
    end

  end

end
If you get the following error when running the above migration with mix ecto.migrate

ERROR (undefined_function): function uuid_generate_v4() does not exist

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
Generate a schema.json file
Please check IN config/config.exs and be sure
-----------------------
create table(:documents) do
  add :title, :string
  add :title, :string, size: 40
  add :title, :string, default: "Hello"
  add :title, :string, default: fragment("now()")
  add :title, :string, null: false
  add :body, :text
  add :age, :integer
  add :price, :float
  add :price, :float, precision: 10, scale: 2
  add :published_at, :datetime
  add :group_id, references(:groups)
  add :object, :json

  timestamps  # inserted_at and updated_at
end
create_if_not_exists table(:documents) do: ... end



config :absinthe,
 schema: CorvesWeb.Schema

mix do app.start, absinthe.schema.json jg_notes/schema.json --pretty


-------------------------------
mix ecto.gen.migration create_opts_sentences
def change do
    create table(:opts_sentences) do
      add :language, :string, size: 2
      add :sentence, :text
      add :note, :text
      add :definition_id, :integer
      add :defs_option_id, references(:defs_options, on_delete: :delete_all)
      timestamps()

    end
    create index("opts_sentences", [:sentence])
  end
  ----------------------------
#mix phx.gen.html Blog Post posts body:string word_count:integer

mix phx.gen.html Canons Opts_sentence opts_sentences \
language:string \
sentence:text \
note:text \
definition_id:integer \
////////////////////////////////////////
mix ecto.gen.migration create_vital_signs

schema "vital_signs" do
    field(:vital_sgns_bmi, :string \
    field(:vital_sgns_body_temp, :string \
    field(:vital_sgns_bp_diast, :string \
    field(:vital_sgns_bp_sys, :string \
    field(:vital_sgns_dt, :date)
    field(:vital_sgns_head_circumference, :string \
    field(:vital_sgns_height, :string \
    field(:vital_sgns_height_lying, :string \
    field(:vital_sgns_notes, :string \
    field(:vital_sgns_pulse_rate, :string \
    field(:vital_sgns_respiratory_rate, :string \
    field(:vital_sgns_sat_val, :string \
    field(:vital_sgns_tm, :time)
    field(:vital_sgns_waist_val, :string \
    field(:vital_sgns_weight_val, :string \
    belongs_to(:care_episode, Corves.Persons.Care_episode)

    timestamps()
  end

  @doc false
  def changeset(vital_sign, attrs) do
    # |> validate_required([:vital_sgns_dt, :vital_sgns_tm, :vital_sgns_bp_sys, :vital_sgns_bp_diast, :vital_sgns_pulse_rate, :vital_sgns_body_temp, :vital_sgns_respiratory_rate, :vital_sgns_sat_val, :vital_sgns_height, :vital_sgns_height_lying, :vital_sgns_weight_val, :vital_sgns_waist_val, :vital_sgns_head_circumference, :vital_sgns_bmi, :vital_sgns_notes])
    vital_sign
    |> cast(attrs, [
      :vital_sgns_dt,
      :vital_sgns_tm,
      :vital_sgns_bp_sys,
      :vital_sgns_bp_diast,
      :vital_sgns_pulse_rate,
      :vital_sgns_body_temp,
      :vital_sgns_respiratory_rate,
      :vital_sgns_sat_val,
      :vital_sgns_height,
      :vital_sgns_height_lying,
      :vital_sgns_weight_val,
      :vital_sgns_waist_val,
      :vital_sgns_head_circumference,
      :vital_sgns_bmi,
      :vital_sgns_notes
    ])
    |> validate_required([])
  end
  ###########################################
mix ecto.gen.migration create_table_sat_pressures

mix phx.gen.html Caths Sat_pressure sat_pressures \
aortic_pres_mean:string \ 
pa_right_pres_mean:string \
pa_pres_sys:string \
rv_pres_dias:string \
svc_pres_dias:string \
ivc_pres_mean:string \
svc_pres_mean:string \
sao2_rv:string \
sao2_ra_mid:string \
ra_pres_x:string \
rv_pres_sys:string \
pa_left_pres_sys:string \
sao2_hepatic_veins:string \
sao2_rv_apex:string \
sat_pres_meds:string \
pcw_pres_dias:string \
sao2_svc_low:string \
sao2_pa:string \
sat_pres_o2_inhalation:string \
lv_pres_sys:string \
ra_pres_v:string \
pcw_pres_x:string \
pa_right_pres_sys:string \
pa_right_pres_dias:string \
ventr_dias_fill_period:string \
pcw_pres_h:string \
pcw_pres_a:string \
sao2_pa_left:string \
pcw_pres_z:string \
ra_pres_mean:string \
pa_pres_mean:string \
sao2_rv_mid:string \
pa_left_pres_mean:string \
sao2_lv:string \
pcw_pres_v:string \
aortic_pres_dias:string \
svc_pres_sys:string \
ventr_sys_ejc_period:string \
sao2_ivc:string \
sat_pres_hb:string \
ivc_pres_sys:string \
pcw_pres_c:string \
aortic_pres_sys:string \
uni_bsa:string \
pcw_pres_mean:string \
ra_pres_h:string \
pa_pres_sys_na:string \
ra_pres_dias:string \
sao2_rv_outflow:string \
ra_pres_a:string \
pa_left_pres_dias:string \
ra_pres_z:string \
uni_qp_qs:string \
pa_pres_mean_na:string \
sat_pres_oxygen_consumption:string \
lv_pres_mean:string \
sao2_ao_distal:string \
sao2_pcw:string \
ivc_pres_dias:string \
lv_pres_end_dias:string \
pa_pres_dias:string \
sao2_ra_low:string \
sat_pres_notes:string \
ra_pres_c:string \
ra_pres_sys:string \
sao2_ra_high:string \
sao2_svc_high:string \
rv_pres_mean:string \
sao2_pa_right:string \
sao2_ao_root:string \
pcw_pres_y:string \
pcw_pres_sys:string \
sat_pulm_veins:string \
sat_pres_start_dt:date \
sat_pres_start_tm:time \
sat_pres_end_dt:date \
sat_pres_end_tm:time \
ra_pres_y:string \
sat_pres_heart_rate:string \
hepatic_veins_pres:string \
patient_id:integer \
info_coronary_id:integer \ 


schema "sat_pressures" do
    field(:aortic_pres_mean, :string \
    field(:pa_right_pres_mean, :string \
    field(:pa_pres_sys, :string \
    field(:rv_pres_dias, :string \
    field(:svc_pres_dias, :string \
    field(:ivc_pres_mean, :string \
    field(:svc_pres_mean, :string \
    field(:sao2_rv, :string \
    field(:sao2_ra_mid, :string \
    field(:ra_pres_x, :string \
    field(:rv_pres_sys, :string \
    field(:pa_left_pres_sys, :string \
    field(:sao2_hepatic_veins, :string \
    field(:sao2_rv_apex, :string \
    field(:sat_pres_meds, :string \
    field(:pcw_pres_dias, :string \
    field(:sao2_svc_low, :string \
    field(:sao2_pa, :string \
    field(:sat_pres_o2_inhalation, :string \
    field(:lv_pres_sys, :string \
    field(:ra_pres_v, :string \
    field(:pcw_pres_x, :string \
    field(:pa_right_pres_sys, :string \
    field(:pa_right_pres_dias, :string \
    field(:ventr_dias_fill_period, :string \
    field(:pcw_pres_h, :string \
    field(:pcw_pres_a, :string \
    field(:sao2_pa_left, :string \
    field(:pcw_pres_z, :string \
    field(:ra_pres_mean, :string \
    field(:pa_pres_mean, :string \
    field(:sao2_rv_mid, :string \
    field(:pa_left_pres_mean, :string \
    field(:sao2_lv, :string \
    field(:pcw_pres_v, :string \
    field(:aortic_pres_dias, :string \
    field(:svc_pres_sys, :string \
    field(:ventr_sys_ejc_period, :string \
    field(:sao2_ivc, :string \
    field(:sat_pres_hb, :string \
    field(:ivc_pres_sys, :string \
    field(:pcw_pres_c, :string \
    field(:aortic_pres_sys, :string \
    field(:uni_bsa, :string \
    field(:pcw_pres_mean, :string \
    field(:ra_pres_h, :string \
    field(:pa_pres_sys_na, :string \
    field(:ra_pres_dias, :string \
    field(:sao2_rv_outflow, :string \
    field(:ra_pres_a, :string \
    field(:pa_left_pres_dias, :string \
    field(:ra_pres_z, :string \
    field(:uni_qp_qs, :string \
    field(:pa_pres_mean_na, :string \
    field(:sat_pres_oxygen_consumption, :string \
    field(:lv_pres_mean, :string \
    field(:sao2_ao_distal, :string \
    field(:sao2_pcw, :string \
    field(:ivc_pres_dias, :string \
    field(:lv_pres_end_dias, :string \
    field(:pa_pres_dias, :string \
    field(:sao2_ra_low, :string \
    field(:sat_pres_notes, :string \
    field(:ra_pres_c, :string \
    field(:ra_pres_sys, :string \
    field(:sao2_ra_high, :string \
    field(:sao2_svc_high, :string \
    field(:rv_pres_mean, :string \
    field(:sao2_pa_right, :string \
    field(:sao2_ao_root, :string \
    field(:pcw_pres_y, :string \
    field(:pcw_pres_sys, :string \
    ### added fields######
    field(:sat_pulm_veins, :string \
    field(:sat_pres_start_dt, :date)
    field(:sat_pres_start_tm, :time)
    field(:sat_pres_end_dt, :date)
    field(:sat_pres_end_tm, :time)
    field(:ra_pres_y, :string \
    field(:sat_pres_heart_rate, :string \
    field(:hepatic_veins_pres, :string \

    ##################
    field(:patient_id, :integer)
    field(:cath_procedure_id, :integer)

    timestamps()
  end

  # added fields
  # :sat_pulm_veins, :sat_pres_start_dt, :sat_pres_start_tt, :sat_pres_end_dt,:sat_pres_end_tt,:ra_pres_y,:sat_pres_heart_rate,

  @doc false
  def changeset(sat_pressure, attrs) do
    sat_pressure
    |> cast(attrs, [
      :aortic_pres_sys,
      :aortic_pres_dias,
      :aortic_pres_mean,
      :sao2_ao_root,
      :sao2_ao_distal,
      :lv_pres_sys,
      :lv_pres_end_dias,
      :lv_pres_mean,
      :sao2_lv,
      :pcw_pres_sys,
      :pcw_pres_dias,
      :pcw_pres_mean,
      :sao2_pcw,
      :pcw_pres_a,
      :pcw_pres_c,
      :pcw_pres_v,
      :pcw_pres_h,
      :pcw_pres_x,
      :pcw_pres_y,
      :pcw_pres_z,
      :pa_pres_mean,
      :pa_pres_sys,
      :pa_pres_dias,
      :sao2_pa,
      :pa_pres_mean_na,
      :pa_pres_sys_na,
      :pa_right_pres_mean,
      :pa_right_pres_sys,
      :pa_right_pres_dias,
      :sao2_pa_right,
      :pa_left_pres_mean,
      :pa_left_pres_sys,
      :pa_left_pres_dias,
      :sao2_pa_left,
      :rv_pres_sys,
      :rv_pres_dias,
      :rv_pres_mean,
      :sao2_rv,
      :sao2_rv_outflow,
      :sao2_rv_mid,
      :sao2_rv_apex,
      :ra_pres_mean,
      :ra_pres_sys,
      :ra_pres_dias,
      :ra_pres_a,
      :ra_pres_c,
      :ra_pres_v,
      :ra_pres_h,
      :ra_pres_x,
      :ra_pres_z,
      :sao2_ra_high,
      :sao2_ra_mid,
      :sao2_ra_low,
      :svc_pres_dias,
      :svc_pres_sys,
      :svc_pres_mean,
      :sao2_svc_low,
      :sao2_svc_high,
      :ivc_pres_sys,
      :ivc_pres_dias,
      :ivc_pres_mean,
      :sao2_ivc,
      :sao2_hepatic_veins,
      :uni_bsa,
      :sat_pres_hb,
      :sat_pres_meds,
      :sat_pres_o2_inhalation,
      :ventr_dias_fill_period,
      :ventr_sys_ejc_period,
      :sat_pres_oxygen_consumption,
      :uni_qp_qs,
      :sat_pres_notes,
      :patient_id,
      :sat_pulm_veins,
      :sat_pres_start_dt,
      :sat_pres_start_tm,
      :sat_pres_end_dt,
      :sat_pres_end_tm,
      :ra_pres_y,
      :hepatic_veins_pres,
      :sat_pres_heart_rate,
      :cath_procedure_id
    ])
    |> validate_required([])

    # |> validate_required([:aortic_pres_sys, :aortic_pres_dias, :aortic_pres_mean, :sao2_ao_root, :sao2_ao_distal, :lv_pres_sys, :lv_pres_end_dias, :lv_pres_mean, :sao2_lv, :pcw_pres_sys, :pcw_pres_dias, :pcw_pres_mean, :sao2_pcw, :pcw_pres_a, :pcw_pres_c, :pcw_pres_v, :pcw_pres_h, :pcw_pres_x, :pcw_pres_y, :pcw_pres_z, :pa_pres_mean, :pa_pres_sys, :pa_pres_dias, :sao2_pa, :pa_pres_mean_na, :pa_pres_sys_na, :pa_right_pres_mean, :pa_right_pres_sys, :pa_right_pres_dias, :sao2_pa_right, :pa_left_pres_mean, :pa_left_pres_sys, :pa_left_pres_dias, :sao2_pa_left, :rv_pres_sys, :rv_pres_dias, :rv_pres_mean, :sao2_rv, :sao2_rv_outflow, :sao2_rv_mid, :sao2_rv_apex, :ra_pres_mean, :ra_pres_sys, :ra_pres_dias, :ra_pres_a, :ra_pres_c, :ra_pres_v, :ra_pres_h, :ra_pres_x, :ra_pres_z, :sao2_ra_high, :sao2_ra_mid, :sao2_ra_low, :svc_pres_dias, :svc_pres_sys, :svc_pres_mean, :sao2_svc_low, :sao2_svc_high, :ivc_pres_sys, :ivc_pres_dias, :ivc_pres_mean, :sao2_ivc, :sao2_hepatic_veins, :uni_bsa, :sat_pres_hb, :cath_lab_meds_yn, :sat_pres_o2_inhalation_yn, :ventr_dias_fill_period, :ventr_sys_ejc_period, :sat_pres_oxygen_consumption, :uni_qp_qs, :sat_pres_notes])
  end
----------------------------

mix phx.gen.html Caths Angio_report angio_reports  --no-schema --no-context

------------------
mix ecto.gen.migration create_vital_signs

