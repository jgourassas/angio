const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const paths = {
  static: path.join(__dirname, '../priv/static'),
  build: path.join(__dirname, '../priv/static/fonts'),
  build: path.join(__dirname, '../priv/static/images'),
  node_modules: path.join(__dirname, '../node_modules'),
  src: path.join(__dirname, './'),
};

module.exports = (env, options) => ({
  optimization: {
    minimizer: [
      new UglifyJsPlugin({cache: true, parallel: true, sourceMap: false}),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  entry: {
    './js/app.js': ['./js/app.js'].concat(glob.sync('./vendor/**/*.js')),
  },
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, '../priv/static/js'),
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
         loader: 'babel-loader',
        },
      },
      {
        test: /\.s?css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
               name: '[name].[ext]',
              outputPath: '../fonts',
              //publicPath: '../priv/static/fonts/'
              // publicPath: '../', // override the default path
            },
          },
        ],
      },
    ],
  },
  //{
  //        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?(\?.*$|$)/,
  //       use: 'url-loader?&limit=10000&name=/fonts/[name].[ext]',
  // },
  // {
  //    test: /\.(eot|ttf|otf)?(\?.*$|$)/,
  //    loader: 'file-loader?&limit=10000&name=/fonts/[name].[ext]',
  //  },
  // ]
  //},
  plugins: [
    new MiniCssExtractPlugin({filename: '../css/app.css'}),
    new MiniCssExtractPlugin({filename: '../css/[name].css'}),
    new CopyWebpackPlugin([{from: 'static/', to: '../'}]),
	//  new webpack.ProvidePlugin({
	  //  $: 'jquery',
	  //  jQuery: 'jquery',
	  // }),
  ],
});
