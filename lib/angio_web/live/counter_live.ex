defmodule AngioWeb.CounterLive do
  use Phoenix.LiveView

  def render(assigns) do
    ~L"""
    <div>
      <h3 class="title is-3",phx-click="boom">The count is: <%= @val %></h3>
      <button phx-click="boom" class="button is-danger">BOOM</button>
      <button class="button is-primary is-medium", phx-click="dec">-</button>
      <button class="button is-primary is-medium" phx-click="inc">+</button>
    </div>

    <span>

    <button class="button is-primary is-small  phx-click="pt_list" >

    Back to Patients LIst
    </button>

    </span>

      <%= if @val < 5 do %>
      <%#= live_render(@socket, AngioWeb.ClockLive) %>
    <% else %>
      <%#= live_render(@socket, AngioWeb.ImageLive) %>
    <% end %>
    """
  end

  def mount(_session, socket) do
    {:ok, assign(socket, :val, 0)}
  end

  @spec handle_event(<<_::24>>, any, Phoenix.LiveView.Socket.t()) :: {:noreply, any}
  def handle_event("inc", _, socket) do
    {:noreply, update(socket, :val, &(&1 + 1))}
  end

  def handle_event("dec", _, socket) do
    {:noreply, update(socket, :val, &(&1 - 1))}
  end

  def handle_event("pt_list", socket) do
    socket
    |> redirect(to: Routes.pt_path(@conn, :index))
  end
end
