defmodule AngioWeb.Router do
  use AngioWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(:fetch_flash)
    plug(Phoenix.LiveView.Flash)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  forward("/graphql", Absinthe.Plug, schema: AngioWeb.Schema)

  forward(
    "/graphiql",
    Absinthe.Plug.GraphiQL,
    schema: AngioWeb.Schema,
    interface: :simple,
    context: %{pubsub: AngioWeb.Endpoint}
  )

  scope "/", AngioWeb do
    pipe_through(:browser)
    live("/counter", CounterLive)

    #    live "/thermostat", ThermostatLive
    # in Index
    # <li><%= link "Thermostat", to: Routes.live_path(@conn, AngioWeb.ThermostatLive) %></li>

    get("/", PageController, :index)
    post("/patients/search_patients", PatientController, :search_patients)
    ########Routes for field Help
    post("/definitions/search_instructions", DefinitionController, :search_instructions)
    post( "/definitions/search_support_definitions",  DefinitionController, :search_support_definitions)
    post("/definitions/search_notes", DefinitionController, :search_notes)
    post("/definitions/search_option_definitions",DefinitionController,:search_option_definitions)
       #####################
    post("/definitions/search_definitions", DefinitionController, :search_definitions)
    post("/defs_tables/search_in_tables", Defs_tableController, :search_in_tables)

    resources "/patients", PatientController, as: :pt do
      # resources("/contacts", ContactController)
      ########## CORONARY ANGIO#################
      resources(
        "/info_coronaries",
        Info_coronaryController,
        only: [:index, :new, :create],
        as: :angio
      )

      resources "/info_coronaries", Info_coronaryController,
        except: [:index, :new, :create],
        as: :angio do
        resources("/care_episodes", Care_episodeController, as: :episode)
        resources("/proc_episodes", Proc_episodeController, as: :proc_episode)
        resources("/cor_collaterals", Cor_collateralController, as: :collateral)
        resources("/cor_anatomies", Cor_anatomyController, as: :anatomy)
        resources("/cor_lesions", Cor_lesionController, as: :lesion)
        resources("/cath_grafts", Cath_graftController, as: :graft)
        resources("/sats_pressures", Sats_pressureController, as: :satpres)

        resources("/angio_report", Angio_reportController, as: :report)
        post("/choose_report", Angio_reportController, :choose_report)
        resources("/Cor_tree", Cor_treeController, as: :cor_tree)
        
        # resources("/reasons", ReasonController)
        # resources("/cath_clinicals", Cath_clinicalController, as: :clinical)
        # resources("/cath_stresses", Cath_stressController, as: :stress)
        resources("/cath_events", Cath_eventController, as: :event)
        # resources("/cath_report", Cath_reportController, as: :cath_report)
        resources("/lventricles", LventricleController, as: :lv)
        resources("/cath_meds", Cath_medController, as: :cathmed)
        resources("/cath_radiations", Cath_radiationController, as: :radiation)
      end

      ## end for info_coronaries
    end

    ## end for partients
    ########## DEFINITIONS OF FIELDS #########
    resources("/defs_tables", Defs_tableController)

    resources "/definitions", DefinitionController do
      resources("/defs_sentences", Defs_sentenceController)
      resources("/defs_codes", Defs_codeController)

      resources "/defs_options", Defs_optionController do
        resources("/opts_codes", Opts_codeController, only: [:index, :new, :create])
        resources("/opts_codes", Opts_codeController)
        resources("/opts_sentences", Opts_sentenceController)
      end
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", AngioWeb do
  #   pipe_through :api
  # end
end
