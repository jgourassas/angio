defmodule Angio.Persons do
  import Ecto.Query, warn: false
  alias Angio.Repo
  alias Angio.Persons.Patient

  def list_patients do
    Repo.all(Patient)
    #  Repo.all(Patient)
  end

  def get_patient!(id), do: Repo.get!(Patient, id)

  def create_patient(attrs \\ %{}) do
    %Patient{}
    |> Patient.changeset(attrs)
    |> Repo.insert()
  end

  def update_patient(%Patient{} = patient, attrs) do
    patient
    |> Patient.changeset(attrs)
    |> Repo.update()
  end

  def delete_patient(%Patient{} = patient) do
    Repo.delete(patient)
  end

  def change_patient(%Patient{} = patient) do
    Patient.changeset(patient, %{})
  end
end
