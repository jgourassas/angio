defmodule AngioPlug do
  use Plug.Builder

  alias Angio.Repo
  alias Angio.Persons.Patient
  alias Angio.Caths.Info_coronary

  plug(:assign_patient)
  plug(:assign_info_coronary)
  plug(:assign_languages)
  plug(:assign_definition)

  def assign_definition(conn, _opts) do
    case conn.params do
      %{"definition_id" => definition_id} ->
        case Angio.Repo.get(Angio.Canons.Definition, definition_id) do
          # nil -> invalid_user(conn)
          definition ->
            assign(conn, :definition, definition)
        end

      _ ->
        conn
    end
  end

  def assign_patient(conn, _opts) do
    case conn.params do
      %{"patient_id" => patient_id} ->
        case Angio.Repo.get(Patient, patient_id) do
          # nil -> invalid_user(conn)
          patient ->
            assign(conn, :patient, patient)
        end

      _ ->
        conn
        |> send_resp(404, "Not Found")
        |> halt()
    end
  end

  def assign_info_coronary(conn, _opts) do
    case conn.params do
      %{"info_coronary_id" => info_coronary_id} ->
        case Repo.get(Info_coronary, info_coronary_id) do
          info_coronary ->
            assign(conn, :info_coronary, info_coronary)
        end

      _ ->
        conn
    end
  end

  #############
  def assign_languages(conn, _) do
    case conn.params do
      %{"language_id" => language_id} ->
        case Repo.get(Language, language_id) do
          languages ->
            assign(conn, :languages, languages)
        end

      _ ->
        conn
    end
  end

  ###################
end

# module
